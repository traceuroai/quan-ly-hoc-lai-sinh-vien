package com.project.qlht.exception;

public class NotClassException extends RuntimeException{

    public NotClassException(Long classId)
    {
        super(String.format("Class `%s` không tồn tại ", classId));
    }

}

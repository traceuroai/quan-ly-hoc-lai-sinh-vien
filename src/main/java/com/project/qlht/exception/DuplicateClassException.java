package com.project.qlht.exception;

public class DuplicateClassException extends DuplicateException{
    public DuplicateClassException(Long classId) {
        super(String.format("Class có id là : `%s` đã tồn tại ", classId));
    }
}

package com.project.qlht.exception;

import com.project.qlht.common.Constant;
import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.common.ResponseTypes;
import org.apache.catalina.core.ApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.engine.spi.SessionImplementor;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.SystemException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.Date;

@Component
public class GenericExceptionMapper implements ExceptionMapper<Exception> {
    private final Log log = LogFactory.getLog(this.getClass());

    @Context
    private HttpServletRequest httpServletRequest;

    @Override
    public Response toResponse(Exception ex) {
        log.error(String.format(ex.getMessage()), ex);
        if (ex instanceof WebApplicationException) return ((WebApplicationException) ex).getResponse();

        if (ex instanceof CustomException) {
            return Response.ok(new ResponseEntity(new Date(), new ResponseError(ResponseTypes.ERROR.getCode(), ex.getMessage()))).build();
        }

        return Response.ok(new ResponseEntity(new Date(), new ResponseError())).build();
    }

}

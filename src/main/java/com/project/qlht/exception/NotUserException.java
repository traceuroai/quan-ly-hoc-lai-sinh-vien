package com.project.qlht.exception;

public class NotUserException extends RuntimeException{

    public NotUserException( )
    {
        super(String.format("Chưa có giáo viên dạy !!! "));
    }

    public NotUserException(Long userId)
    {
        super(String.format("Không tồn tại người dùng : `%s` ", userId));
    }

    public NotUserException(String userName)
    {
        super(String.format("Không tồn tại người dùng : `%s` ", userName));
    }

}

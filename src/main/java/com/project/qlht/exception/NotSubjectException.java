package com.project.qlht.exception;

public class NotSubjectException extends RuntimeException{

    public NotSubjectException(Long subjectId)
    {
        super(String.format("Không tồn tại môn học : `%s` ", subjectId));
    }

}

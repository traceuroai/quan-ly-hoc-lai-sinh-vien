package com.project.qlht.exception;

public class NotTranscriptException extends RuntimeException{

    public NotTranscriptException(Long transcriptId)
    {
        super(String.format("Không tồn tại bảng điểm : `%s` ", transcriptId));
    }

}

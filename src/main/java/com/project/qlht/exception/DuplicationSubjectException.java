package com.project.qlht.exception;

public class DuplicationSubjectException extends DuplicateException{

    public DuplicationSubjectException(Long subjectId) {
        super(String.format("Môn học có id là : `%s` đã tồn tại ", subjectId));
    }

}

package com.project.qlht.exception;

public class StudyTimeException extends RuntimeException{

    public StudyTimeException(int lessNumber)
    {
        super(String.format("Số buổi học không tương ứng : `%s` ", lessNumber));
    }

}

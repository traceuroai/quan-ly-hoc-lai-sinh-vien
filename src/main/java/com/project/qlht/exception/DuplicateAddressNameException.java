package com.project.qlht.exception;

public class DuplicateAddressNameException extends DuplicateException{

    public DuplicateAddressNameException(String addressName)
    {
        super(String.format("Dịa chỉ `%s` đã tồn tại ", addressName));
    }

}

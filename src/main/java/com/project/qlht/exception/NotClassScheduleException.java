package com.project.qlht.exception;

public class NotClassScheduleException extends RuntimeException{

    public NotClassScheduleException(Long classScheduleId)
    {
        super(String.format("Không tồn tại lịch học `%s` ", classScheduleId));
    }

    public NotClassScheduleException()
    {
        super(String.format("Không có lịch học"));
    }

    public NotClassScheduleException(Long classScheduleId, Long userId)
    {
        super(String.format("Không có lịch dạy môn `%s` của giáo viên :  `%s` ", classScheduleId, userId ));
    }

//    public NotClassScheduleException(Long subjectId)
//    {
//        super(String.format("Không tồn tại lịch học có mã môn học là ", subjectId));
//    }

}

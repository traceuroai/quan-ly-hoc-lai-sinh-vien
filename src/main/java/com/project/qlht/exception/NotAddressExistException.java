package com.project.qlht.exception;

public class NotAddressExistException extends CustomException{

    public NotAddressExistException(Long idAddress)
    {
        super(String.format("Địa chỉ `%s` không tồn tại ", idAddress));
    }

}

package com.project.qlht.controller;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.AddressDto;
import com.project.qlht.dto.CustomUserDetails;
import com.project.qlht.dto.SemesterDto;
import com.project.qlht.service.AddressService;
import com.project.qlht.service.SemesterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "semester")
public class SemesterController {

    @Autowired
    private SemesterService semesterService;

    @GetMapping(value = "list")
    public ResponseEntity getListAddress(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                         @RequestParam(name = "name", required = false) String name,
                                         @RequestParam(name = "pageIndex", defaultValue = "1") Integer pageindex,
                                         @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize)
    {
        return new ResponseEntity(new ResponseError(200, "Success"),
                semesterService.findAll(pageindex, pageSize, customUserDetails.getUsername(), name));
    }

    @GetMapping(value = "detail")
    public ResponseEntity detail(@RequestParam(name = "id") Long id)
    {
        return new ResponseEntity(new ResponseError(200, "Success"),
                semesterService.findOneById(id));
    }

    @PostMapping(value = "add")
    public ResponseEntity addAddress(@NotNull(message = "body must be not null") @RequestBody SemesterDto semesterDto)
    {
        semesterService.save(semesterDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @PostMapping(value = "update")
    public ResponseEntity updateAddress(@NotNull(message = "body must be not null") @RequestBody SemesterDto semesterDto)
    {
        semesterService.update(semesterDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @GetMapping(value = "delete")
    public ResponseEntity delete(@NotNull @RequestParam(value = "id") Long id)
    {
        semesterService.delete(id);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

}

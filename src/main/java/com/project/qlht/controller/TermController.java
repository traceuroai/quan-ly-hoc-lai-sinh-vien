package com.project.qlht.controller;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.TermDto;
import com.project.qlht.service.TermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "term")
public class TermController {

    @Autowired
    private TermService termService;

    // DANH SÁCH HỌC PHẦN
    @GetMapping(value = "list")
    public ResponseEntity listSubject(@RequestParam(name = "pageIndex", defaultValue = "1") Integer pageIndex,
                                      @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                                      @RequestParam(name = "name", required = false) String name) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                termService.findAll(pageIndex, pageSize, name));
    }

    // CHI TIẾT HỌC PHẦN
    @GetMapping(value = "detail")
    public ResponseEntity listSubject(@RequestParam(name = "id") Long id) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                termService.detail(id));
    }

    // THÊM HỌC PHẦN
    @PostMapping(value = "add")
    public ResponseEntity add(@NotNull(message = "body must be not null") @RequestBody TermDto termDto) {
        termService.save(termDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    // CẬP NHẬT HỌC PHẦN
    @PostMapping(value = "update")
    public ResponseEntity update(@NotNull(message = "body must be not null") @RequestBody TermDto termDto) {
        termService.update(termDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    // XÓA HỌC PHẦN
    @GetMapping(value = "delete")
        public ResponseEntity delete(@NotNull @RequestParam(value = "termId") Long termId) {
        termService.delete(termId);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }


}

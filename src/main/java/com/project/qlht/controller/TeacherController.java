package com.project.qlht.controller;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.CustomUserDetails;
import com.project.qlht.dto.detail.UserDetailDto;
import com.project.qlht.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;

@RestController
@RequestMapping(value = "teacher")
public class TeacherController {

    @Autowired
    private UserService userService;

    // DANH SÁCH GIÁO VIÊN
    @GetMapping(value = "list")
    public ResponseEntity listTeacher(@RequestParam(name = "pageIndex", defaultValue = "1") Integer pageIndex,
                                      @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                                      @RequestParam(name = "name", required = false) String name) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                userService.findTeacher(pageIndex, pageSize, name));
    }

    // CHI TIẾT GIÁO VIÊN
    @GetMapping(value = "detail")
    public ResponseEntity detailTeacher(@RequestParam(name = "id") Long id) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                userService.detailTeacher(id));
    }

    // THÊM GIÁO VIÊN
    @PostMapping(value = "add")
    public ResponseEntity add(@NotNull(message = "body must be not null") @RequestBody UserDetailDto userDetailDto,
                              @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        userDetailDto.setCreatedDate(new Date());
        userDetailDto.setCreatedBy(customUserDetails.getUsername());
        userService.addTeacher(userDetailDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    // CẬP NHẬT GIÁO VIÊN
    @PostMapping(value = "update")
    public ResponseEntity update(@NotNull(message = "body must be not null") @RequestBody UserDetailDto userDetailDto,
                                 @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        userDetailDto.setModifiedDate(new Date());
        userDetailDto.setModifiedBy(customUserDetails.getUsername());
        userService.updateTeacher(userDetailDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    // XÓA GIÁO VIÊN
    @GetMapping(value = "delete")
    public ResponseEntity delete(@NotNull @RequestParam(value = "teacherId") Long teacherId) {
        userService.delete(teacherId);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

}

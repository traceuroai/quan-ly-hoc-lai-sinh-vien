package com.project.qlht.controller;

import java.util.Date;
import javax.validation.constraints.NotNull;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import com.project.qlht.dto.RegisterDto;
import com.project.qlht.service.RegisterService;

@RestController
@RequestMapping(value = "register")
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @GetMapping(value = "list")
    public ResponseEntity list(@RequestParam(name = "pageIndex", defaultValue = "1") Integer pageIndex,
                               @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                               @RequestParam(name = "subjectId", required = false) Long subjectId,
                               @RequestParam(name = "userId", required = false) Long userId) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                registerService.findAll(pageIndex, pageSize, subjectId, userId));
    }

    @GetMapping(value = "detail")
    public ResponseEntity detail(@RequestParam(name = "id") Long id) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                registerService.detail(id));
    }

    @PostMapping(value = "add")
    public ResponseEntity add(@NotNull(message = "body must be not null") @RequestBody RegisterDto registerDto,
                              @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        registerDto.setCreatedDate(new Date());
        registerDto.setCreatedBy(customUserDetails.getUsername());
        registerService.save(registerDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @PostMapping(value = "update")
    public ResponseEntity update(@NotNull(message = "body must be not null") @RequestBody RegisterDto registerDto,
                                 @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        registerDto.setModifiedDate(new Date());
        registerDto.setModifiedBy(customUserDetails.getUsername());
        registerService.update(registerDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @GetMapping(value = "delete")
    public ResponseEntity delete(@NotNull @RequestParam(value = "id") Long id) {
        registerService.delete(id);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

}

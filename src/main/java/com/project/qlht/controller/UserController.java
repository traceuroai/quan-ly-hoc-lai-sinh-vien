package com.project.qlht.controller;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseTypes;
import com.project.qlht.dto.*;
import com.project.qlht.dto.detail.UserDetailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.qlht.service.UserService;

@RestController
@RequestMapping(value = "user")
@PermitAll
public class UserController {
	@Autowired
	private UserService userService;

	@PostMapping("login")
	public ResponseEntity authenticateUser(@Valid @RequestBody LoginRequest user) {

		return new ResponseEntity(ResponseTypes.SUCCESS, userService.login(user));
	}

	@PostMapping("register")
	public ResponseEntity register(@Valid @RequestBody UserDetailDto user) {
		return new ResponseEntity(ResponseTypes.SUCCESS, userService.add(user));
	}

}

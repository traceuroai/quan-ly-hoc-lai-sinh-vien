package com.project.qlht.controller;

import javax.validation.constraints.NotNull;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.ClassDto;
import com.project.qlht.dto.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import com.project.qlht.service.ClassService;

import java.util.Date;

@RestController
@RequestMapping(value = "class")
public class ClassController {

    @Autowired
    private ClassService classService;

    // DANH SÁCH LỚP HỌC
    @GetMapping(value = "list")
    public ResponseEntity listClass(@RequestParam(name = "name", required = false) String name,
                                    @RequestParam(name = "course", required = false) Integer course,
                                    @RequestParam(name = "pageIndex", required = false, defaultValue = "1") Integer pageIndex,
                                    @RequestParam(name = "pageSize", required = false, defaultValue = "20") Integer pageSize) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                classService.findAll(pageIndex, pageSize, name, course));
    }

    // CHI TIẾT LỚP HỌC
    @GetMapping(value = "detail")
    public ResponseEntity detail(@RequestParam(name = "classId") Long classId) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                classService.findOneById(classId));
    }

    // THÊM LỚP HỌC
    @PostMapping(value = "add")
    public ResponseEntity add(@NotNull(message = "body must be not null") @RequestBody ClassDto classDto,
                              @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        classDto.setCreateddate(new Date());
        classDto.setCreatedBy(customUserDetails.getUsername());
        classService.save(classDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    // CẬP NHẬT LỚP HỌC
    @PostMapping(value = "update")
    public ResponseEntity update(@NotNull(message = "body must be not null") @RequestBody ClassDto classDto,
                                 @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        classDto.setModifiedDate(new Date());
        classDto.setModifiedBy(customUserDetails.getUsername());
        classService.update(classDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    // XÓA LỚP HỌC
    @GetMapping(value = "delete")
    public ResponseEntity delete(@NotNull @RequestParam(value = "classId") Long classId) {
        classService.delete(classId);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

}

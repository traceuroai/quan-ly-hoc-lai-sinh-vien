package com.project.qlht.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.qlht.dto.ResponseHttp;
import com.project.qlht.dto.RoleDto;
import com.project.qlht.service.RoleService;

@RestController
@RequestMapping(value = "/role/")
public class RoleController {
	
	ResponseHttp responseHttp;

	@Autowired
	private RoleService roleService;
	
	@GetMapping(value = "list", consumes = "application/json", produces = "application/json")
	public Object listRole (HttpServletResponse resp)
	{
		if(roleService.findAll() == null)
		{
			responseHttp = new ResponseHttp("select is failer !!!", HttpServletResponse.SC_FOUND);
			return responseHttp.getStringResponse();
		}
		return roleService.findAll();
	}
	
	@PostMapping(value = "role/{id}", consumes = "application/json", produces = "application/json")
	public Object findOneRole(@PathVariable(name = "id") Long id, HttpServletResponse resp)
	{
		RoleDto roleDto = roleService.findOneById(id);
		if(roleDto == null)
		{
			responseHttp = new ResponseHttp("address does not exist", HttpServletResponse.SC_UNAUTHORIZED);
			return responseHttp.getStringResponse();
		}
		return roleDto;
	}
	
	@PostMapping(value = "role", consumes = "application/json", produces = "application/json")
	public Object addRole(@RequestBody RoleDto roleDto)
	{
		RoleDto isRole = roleService.save(roleDto);
		if(isRole == null)
		{
			responseHttp = new ResponseHttp("add Role is failer !!!", HttpServletResponse.SC_UNAUTHORIZED);
			return responseHttp.getStringResponse();
		}
		return isRole;
	}
	
	@PutMapping(value = "role/{id}", consumes = "application/json", produces = "application/json")
	public Object updateRole(@RequestBody RoleDto roleDto, @PathVariable("id") Long id)// chú ý Id : Long 
	{
		roleDto.setId(id);
		RoleDto isRole = roleService.update(roleDto);
		if(isRole == null)
		{
			responseHttp = new ResponseHttp("update Role is failer !!!", HttpServletResponse.SC_UNAUTHORIZED);
			return responseHttp.getMessage();
		}
		return isRole;
	}
	
	@DeleteMapping(value = "role/{id}", consumes = "application/json", produces = "application/json")
	public String deleteRole(@PathVariable(name = "id") Long id , HttpServletResponse resp)
	{
		return roleService.delete(id);
	}

}

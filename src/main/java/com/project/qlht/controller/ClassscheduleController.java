package com.project.qlht.controller;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.CustomUserDetails;
import com.project.qlht.dto.detail.ClassScheduleDetailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import com.project.qlht.service.ClassscheduleService;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "classSchedule")
public class ClassscheduleController {

    @Autowired
    private ClassscheduleService classScheduleService;

    @GetMapping(value = "list")
    public ResponseEntity listClassSchedule(@RequestParam(value = "subject", required = false) Long subjectId,
                                            @RequestParam(value = "teacher", required = false) Long teacherId,
                                            @RequestParam(value = "pageIndex", required = false, defaultValue = "1") Integer pageIndex,
                                            @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                classScheduleService.findAll(subjectId, teacherId, pageIndex, pageSize));
    }

    @GetMapping(value = "detail")
    public ResponseEntity detail(@RequestParam("classScheduleId") Long classScheduleId) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                classScheduleService.detail(classScheduleId));
    }

    @PostMapping(value = "add")
    public ResponseEntity addClassSchedule(@RequestBody(required = false) ClassScheduleDetailDto classScheduleDetailDto,
                                           @AuthenticationPrincipal CustomUserDetails customUserDetails)
    {
        classScheduleService.add(classScheduleDetailDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @PostMapping(value = "update")
    public ResponseEntity updateClassSchedule(@RequestBody(required = false) ClassScheduleDetailDto ClassScheduleDetailDto,
                                              @AuthenticationPrincipal CustomUserDetails customUserDetails)
    {
        classScheduleService.update(ClassScheduleDetailDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @GetMapping(value = "delete")
    public ResponseEntity deleteClassSchedule(@NotNull @RequestParam(value = "classScheduleId") Long id,
                                              @AuthenticationPrincipal CustomUserDetails customUserDetails)
    {
        classScheduleService.delete(id);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @GetMapping(value = "student/search")
    public ResponseEntity studentSearchClassSchedule(@NotNull @RequestParam(value = "subjectId") Long subjectId,
                                              @AuthenticationPrincipal CustomUserDetails customUserDetails)
    {
        return new ResponseEntity(new ResponseError(200, "Success"), classScheduleService.search(subjectId));
    }

    @GetMapping(value = "teacher/search")
    public ResponseEntity teacherSearchClassSchedule(@NotNull @RequestParam(value = "subjectId") Long subjectId,
                                                     @AuthenticationPrincipal CustomUserDetails customUserDetails)
    {
        return new ResponseEntity(new ResponseError(200, "Success"),
                classScheduleService.search2(subjectId, customUserDetails.getUser().getId()));
    }


}

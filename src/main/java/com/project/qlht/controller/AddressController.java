package com.project.qlht.controller;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.AddressDto;
import com.project.qlht.dto.CustomUserDetails;
import com.project.qlht.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Date;

@RestController
@RequestMapping(value = "address")
public class AddressController {
		
    @Autowired
    private AddressService addressService;

    @GetMapping(value = "list")
    public ResponseEntity getListAddress(@AuthenticationPrincipal CustomUserDetails customUserDetails,
                                         @RequestParam(name = "name", required = false) String name,
                                         @RequestParam(name = "pageIndex", required = false, defaultValue = "1") Integer pageindex,
                                         @RequestParam(name = "pageSize", required = false, defaultValue = "20") Integer pageSize)
    {
        return new ResponseEntity(new ResponseError(200, "Success"),
                addressService.findAll(pageindex, pageSize, customUserDetails.getUsername(), name));
    }

    @GetMapping(value = "detail")
    public ResponseEntity detail(@RequestParam(name = "addressId") Long addressId)
    {
        return new ResponseEntity(new ResponseError(200, "Success"),
                addressService.findOneById(addressId));
    }

    @PostMapping(value = "add")
    public ResponseEntity addAddress(@NotNull(message = "body must be not null") @RequestBody AddressDto addressDto,
                                     @AuthenticationPrincipal CustomUserDetails customUserDetails)
    {
        addressService.save(addressDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @PostMapping(value = "update")
    public ResponseEntity updateAddress(@NotNull(message = "body must be not null") @RequestBody AddressDto addressDto,
                                        @AuthenticationPrincipal CustomUserDetails customUserDetails)
    {
        addressService.update(addressDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @GetMapping(value = "delete")
    public ResponseEntity delete(@NotNull @RequestParam(value = "addressId") Long id)
    {
        addressService.delete(id);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }
}

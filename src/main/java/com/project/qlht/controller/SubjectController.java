package com.project.qlht.controller;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.CustomUserDetails;
import com.project.qlht.dto.SubjectDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import com.project.qlht.service.SubjectService;

import javax.validation.constraints.NotNull;
import java.util.Date;

@RestController
@RequestMapping(value = "subject")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    // DANH SÁCH MÔN HỌC
    @GetMapping(value = "list")
    public ResponseEntity listSubject(@RequestParam(name = "pageIndex", defaultValue = "1") Integer pageIndex,
                                      @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                                      @RequestParam(name = "name", required = false) String name,
                                      @RequestParam(name = "code", required = false) String code,
                                      @RequestParam(name = "course", required = false) Long course,
                                      @RequestParam(name = "semester", required = false) Long semester) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                subjectService.findAll(pageIndex, pageSize, name, code, course, semester));
    }

    // CHI TIẾT MÔN HỌC
    @GetMapping(value = "detail")
    public ResponseEntity listSubject(@RequestParam(name = "id") Long id) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                subjectService.detail(id));
    }

    // THÊM MÔN HỌC
    @PostMapping(value = "add")
    public ResponseEntity add(@NotNull(message = "body must be not null") @RequestBody SubjectDto subjectDto,
                              @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        subjectDto.setCreatedDate(new Date());
        subjectDto.setCreatedBy(customUserDetails.getUsername());
        subjectService.save(subjectDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    // CẬP NHẬT MÔN HỌC
    @PostMapping(value = "update")
    public ResponseEntity update(@NotNull(message = "body must be not null") @RequestBody SubjectDto subjectDto,
                                 @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        subjectDto.setModifiedDate(new Date());
        subjectDto.setModifiedBy(customUserDetails.getUsername());
        subjectService.update(subjectDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    // XÓA MÔN HỌC
    @GetMapping(value = "delete")
    public ResponseEntity delete(@NotNull @RequestParam(value = "subjectId") Long subjectId) {
        subjectService.delete(subjectId);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }
}

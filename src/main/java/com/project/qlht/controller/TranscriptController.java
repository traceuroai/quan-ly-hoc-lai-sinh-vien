package com.project.qlht.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.CustomUserDetails;
import com.project.qlht.dto.detail.TranscriptDetailDto;
import com.project.qlht.service.TranscriptDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import com.project.qlht.service.TranscriptService;

@CrossOrigin
@RestController
@RequestMapping(value = "transcript")
public class TranscriptController {

	@Autowired
	private TranscriptService transcriptService;
	@Autowired
	private TranscriptDetailService transcriptDetailService;

	@GetMapping(value = "create")
	public ResponseEntity createTranscript(@RequestParam(name = "subjectId") Long subjectId,
										   @AuthenticationPrincipal CustomUserDetails customUserDetails)
	{
		return new ResponseEntity(new Date().toString(), transcriptService.create(subjectId, customUserDetails.getUser().getId()));
	}

	@GetMapping(value = "list")
	public ResponseEntity listTranscript(@AuthenticationPrincipal CustomUserDetails customUserDetails)
	{
		return new ResponseEntity(new Date().toString(), transcriptService.findAll());
	}

	@GetMapping(value = "detail")
	public ResponseEntity detailTranscript(@RequestParam(name = "transcriptId") Long transcriptId,
										   @AuthenticationPrincipal CustomUserDetails customUserDetails)
	{
		// userId : id giáo viên tạo bảng điểm
		return new ResponseEntity(new Date().toString(), transcriptService.detail(transcriptId));
	}

	@PostMapping(value = "update")
	public Object updateTranscript(@RequestBody List<TranscriptDetailDto> transcripts,
								   @AuthenticationPrincipal CustomUserDetails customUserDetails)
	{
		transcriptService.update(transcripts);
		return new ResponseEntity(new Date().toString(), new Object());
	}

	// tra cứu điểm
	@GetMapping(value = "search")
	public ResponseEntity searchTranscript(/*@RequestParam(name = "userId") Long userId,*/
										   @RequestParam(name = "subjectId") Long subjectId,
										   @AuthenticationPrincipal CustomUserDetails customUserDetails)
	{
		System.err.println("User Id : " + customUserDetails.getUser().getId());
		return new ResponseEntity(new Date().toString(), transcriptService.search(customUserDetails.getUser().getId(), subjectId));
	}

	@GetMapping(value = "deleteTranscript")
	public ResponseEntity deleteTranscript(@RequestParam(name = "transcriptId") Long id)
	{
		transcriptService.delete(id);
		return new ResponseEntity(new ResponseError(200, "Success"), new Object());
	}

	@GetMapping(value = "delete")
	public ResponseEntity deleteTranscriptDetail(@RequestParam(name = "transcriptDetailId") Long id)
	{
		return null;
	}

}

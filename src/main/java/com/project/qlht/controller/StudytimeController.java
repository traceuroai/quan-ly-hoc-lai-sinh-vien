package com.project.qlht.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.qlht.dto.ResponseHttp;
import com.project.qlht.dto.StudytimeDto;
import com.project.qlht.service.StudytimeService;

@RestController
@RequestMapping(value = "/studyTime/")
public class StudytimeController {
	
	ResponseHttp responseHttp;
	
	@Autowired
	private StudytimeService studyTimeService;

	@GetMapping(value = "list", consumes = "application/json", produces = "application/json")
	public Object listStudyTime (HttpServletResponse resp)
	{
		List<StudytimeDto> listStudytimeDtos = studyTimeService.findAll();
		if(listStudytimeDtos == null)
		{
			responseHttp = new ResponseHttp("select is failer", HttpServletResponse.SC_NOT_IMPLEMENTED);
			return responseHttp.getStringResponse();
		}
		return listStudytimeDtos;
	}
	
	@PostMapping(value = "studyTime/{id}", consumes = "application/json", produces = "application/json")
	public Object findOneStudyTime(@PathVariable(name = "id") Long id, HttpServletResponse resp)
	{
		StudytimeDto studytimeDto = studyTimeService.findOneById(id);
		if(studytimeDto == null)
		{
			responseHttp = new ResponseHttp("select is failer", HttpServletResponse.SC_NOT_IMPLEMENTED);
			responseHttp.getStringResponse();
		}
		return studytimeDto;
	}
	
	@PostMapping(value = "studyTime", consumes = "application/json", produces = "application/json")
	public Object addStudyTime(@RequestBody StudytimeDto studytimeDto)
	{
		StudytimeDto isStudytime = studyTimeService.save(studytimeDto);
		if(isStudytime == null)
		{
			responseHttp = new ResponseHttp("add Study Time is failer !!!", HttpServletResponse.SC_UNAUTHORIZED);
			return responseHttp.getStringResponse();
		}
		return isStudytime;
	}
	
	@PutMapping(value = "studyTime/{id}", consumes = "application/json", produces = "application/json")
	public Object updateStudyTime(@RequestBody StudytimeDto studytimeDto, @PathVariable("id") Long id)
	{
		studytimeDto.setId(id);
		StudytimeDto isStudytime = studyTimeService.update(studytimeDto);
		if(isStudytime == null)
		{
			responseHttp = new ResponseHttp("update Study Time is failer !!!", HttpServletResponse.SC_UNAUTHORIZED);
			return responseHttp.getStringResponse();
		}
		return isStudytime;
	}
	
	@DeleteMapping(value = "studyTime/{id}", consumes = "application/json", produces = "application/json")
	public String deleteStudyTime(@PathVariable(name = "id") Long id, HttpServletResponse resp)
	{
		return studyTimeService.delete(id);
	}
	
}

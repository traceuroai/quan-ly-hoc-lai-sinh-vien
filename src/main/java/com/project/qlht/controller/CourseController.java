package com.project.qlht.controller;


import com.project.qlht.common.ResponseEntity;
import com.project.qlht.common.ResponseError;
import com.project.qlht.dto.CourseDto;
import com.project.qlht.dto.CustomUserDetails;
import com.project.qlht.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "course")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping(value = "list")
    public ResponseEntity listClass(@RequestParam(name = "name", required = false) String name,
                                    @RequestParam(name = "code", required = false) String code,
                                    @RequestParam(name = "pageIndex", required = false, defaultValue = "1") Integer pageIndex,
                                    @RequestParam(name = "pageSize", required = false, defaultValue = "20") Integer pageSize) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                courseService.findAll(pageIndex, pageSize, name, code));
    }

    @GetMapping(value = "detail")
    public ResponseEntity detail(@RequestParam(name = "courseId") Long courseId) {
        return new ResponseEntity(new ResponseError(200, "Success"),
                courseService.findOneById(courseId));
    }

    @PostMapping(value = "add")
    public ResponseEntity add(@NotNull(message = "body must be not null") @RequestBody CourseDto courseDto) {
        courseService.save(courseDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @PostMapping(value = "update")
    public ResponseEntity update(@NotNull(message = "body must be not null") @RequestBody CourseDto courseDto,
                                 @AuthenticationPrincipal CustomUserDetails customUserDetails) {
        courseService.update(courseDto);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

    @GetMapping(value = "delete")
    public ResponseEntity delete(@NotNull @RequestParam(value = "courseId") Long courseId) {
        courseService.delete(courseId);
        return new ResponseEntity(new ResponseError(200, "Success"), new Object());
    }

}

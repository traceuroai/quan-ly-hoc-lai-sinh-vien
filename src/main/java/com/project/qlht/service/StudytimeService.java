package com.project.qlht.service;

import java.util.List;

import com.project.qlht.dto.StudytimeDto;

public interface StudytimeService {
	
	List<StudytimeDto> findAll();

	StudytimeDto findOneById(Long id);

	StudytimeDto save(StudytimeDto studytimeDto);

	StudytimeDto update(StudytimeDto studytimeDto);

	String delete(Long id);

}

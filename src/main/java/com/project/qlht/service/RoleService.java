package com.project.qlht.service;

import java.util.List;

import com.project.qlht.dto.RoleDto;

public interface RoleService {
	
	List<RoleDto> findAll();

	RoleDto findOneById(Long id);

	RoleDto save(RoleDto roleDto);

	RoleDto update(RoleDto roleDto);

	String delete(Long id);

}

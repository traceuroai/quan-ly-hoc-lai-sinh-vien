package com.project.qlht.service;

import java.util.List;

import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.AddressDto;

public interface AddressService {

	PageDto<AddressDto> findAll(Integer pageIndex, Integer pageSize, String userName, String nameAddress);

	AddressDto findOneById(Long id);

	void save(AddressDto addressDto);

	void update(AddressDto addressDto);

	void delete(Long id);

}

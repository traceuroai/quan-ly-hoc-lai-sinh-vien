package com.project.qlht.service;
import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.ClassScheduleDto;
import com.project.qlht.dto.detail.ClassScheduleDetailDto;

public interface ClassscheduleService {

	PageDto<ClassScheduleDto> findAll(Long subjectId, Long teacherId, Integer pageIndex, Integer pageSize);

	ClassScheduleDetailDto detail(Long id);

	void add(ClassScheduleDetailDto classScheduleDetailDto);

	void update(ClassScheduleDetailDto classScheduleDetailDto);

	ClassScheduleDto search(Long subjectId);

	ClassScheduleDto search2(Long subjectId, Long userId);

	void delete(Long id);

}

package com.project.qlht.service;

import java.util.List;

import com.project.qlht.dto.TranscriptDto;
import com.project.qlht.dto.detail.TranscriptDetailDto;

public interface TranscriptService {

	List<TranscriptDto> findAll();

	TranscriptDto detail(long id);

	TranscriptDto create(Long subjectId, Long userId);

	List<TranscriptDetailDto> update(List<TranscriptDetailDto> transcripts);

	TranscriptDetailDto search(Long userId, Long subjectId);

	String delete(Long id);
	
}

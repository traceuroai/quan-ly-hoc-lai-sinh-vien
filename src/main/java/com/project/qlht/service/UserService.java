package com.project.qlht.service;

import java.util.List;

import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.LoginRequest;
import com.project.qlht.dto.SubjectDto;
import com.project.qlht.dto.detail.UserDetailDto;
import com.project.qlht.dto.UserDto;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService{

	List<UserDto> findAll();

	UserDetails loadUserById(Long id);

	UserDto login(LoginRequest loginRequest);

	UserDto add(UserDetailDto userDetailDto);

	UserDetails update(UserDetailDto userDetailDto);

	void delete(Long id);

	PageDto<UserDto> findTeacher(Integer pageIndex, Integer pageSize, String name);

	UserDetailDto detailTeacher(Long teacherId);

	UserDto addTeacher(UserDetailDto userDetailDto);

	UserDto updateTeacher(UserDetailDto userDetailDto);

	PageDto<UserDto> findStudent(Integer pageIndex, Integer pageSize, String name);

	UserDetailDto detailStudent(Long teacherId);

	UserDto addStudent(UserDetailDto userDetailDto);

	UserDto updateStudent(UserDetailDto userDetailDto);
	
}

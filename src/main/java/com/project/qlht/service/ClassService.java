package com.project.qlht.service;

import java.util.List;

import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.ClassDto;

public interface ClassService {
	
	PageDto<ClassDto> findAll(Integer pageIndex, Integer pageSize, String name, Integer course);

	ClassDto findOneById(Long id);

	void save(ClassDto classDto);

	void update(ClassDto classDto);

	void delete(Long id);

}

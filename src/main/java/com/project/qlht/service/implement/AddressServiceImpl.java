package com.project.qlht.service.implement;

import java.util.List;
import java.util.stream.Collectors;

import com.project.qlht.constant.Address_;
import com.project.qlht.constant.PageDto;
import com.project.qlht.converter.AddressConverter;
import com.project.qlht.entity.Address;
import com.project.qlht.exception.DuplicateAddressNameException;
import com.project.qlht.exception.NotAddressExistException;
import com.project.qlht.repository.AddressRepo;
import com.project.qlht.utils.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.project.qlht.dto.AddressDto;
import com.project.qlht.service.AddressService;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AddressServiceImpl implements AddressService{

	@Autowired
	private AddressRepo addressRepo;

	@Autowired
	private AddressConverter addressConverter;

	@Override
	public PageDto<AddressDto> findAll(Integer pageIndex, Integer pageSize, String userName, String nameAddress) {

		Specification<Address> specification = (root, cq, cb) -> cb.and(
				QueryUtils.buildLikeFilter(root, cb, nameAddress, Address_.NAME)
		);

		Page<Address> addressEntityPage = addressRepo.findAll(specification,
				PageRequest.of(pageIndex - 1, pageSize));

		List<AddressDto> addressDtos = addressEntityPage.getContent().stream().map(addressEntity ->
				AddressDto.builder().address(addressEntity).build()).collect(Collectors.toList());

		return PageDto.of(addressEntityPage, addressDtos);
	}

	@Override
	public AddressDto findOneById(Long id) {

		Address addressEntity = addressRepo.findById(id).orElseThrow(() ->
				new NotAddressExistException(id));

		return AddressDto.builder().address(addressEntity).build();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void save(AddressDto addressDto) {
		if(addressRepo.existsByAddress(addressDto.getAddress()))
		{
			throw new DuplicateAddressNameException(addressDto.getAddress());
		}
		addressRepo.save(addressConverter.toAddress(addressDto));
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(AddressDto addressDto) {

		Address addressEntity = addressDto.getId() != null ? addressRepo.findById(addressDto.getId()).orElseThrow(() ->
				new NotAddressExistException(addressDto.getId())) : null;

		if (addressEntity != null && addressRepo.existsByAddress(addressDto.getAddress()) &&
				!addressEntity.getAddress().equals(addressDto.getAddress().trim()))
			throw new DuplicateAddressNameException(addressDto.getAddress());

		addressRepo.save(addressConverter.toAddress(addressDto));
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void delete(Long id) {
		Address addressEntity = addressRepo.findById(id)
				.orElseThrow(() -> new NotAddressExistException(id));
		addressRepo.delete(addressEntity);
	}
}

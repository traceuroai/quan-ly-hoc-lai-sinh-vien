package com.project.qlht.service.implement;

import com.project.qlht.constant.Class_;
import com.project.qlht.constant.PageDto;
import com.project.qlht.converter.CourseConverter;
import com.project.qlht.dto.CourseDto;
import com.project.qlht.entity.Class;
import com.project.qlht.entity.Course;
import com.project.qlht.exception.CustomException;
import com.project.qlht.exception.DuplicateClassException;
import com.project.qlht.repository.ClassRepo;
import com.project.qlht.repository.CourseRepo;
import com.project.qlht.service.CourseService;
import com.project.qlht.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {


    private final CourseRepo courseRepo;
    private final CourseConverter courseConverter;
    private final ClassRepo classRepo;

    public CourseServiceImpl(CourseRepo courseRepo, CourseConverter courseConverter, ClassRepo classRepo) {
        this.courseRepo = courseRepo;
        this.courseConverter = courseConverter;
        this.classRepo = classRepo;
    }

    @Override
    public PageDto<CourseDto> findAll(Integer pageIndex, Integer pageSize, String name, String code) {

        Specification<Course> specification = (root, cq, cb) -> cb.and(
                QueryUtils.buildLikeFilter(root, cb, name, Class_.NAME),
                QueryUtils.buildLikeFilter(root, cb, code, Class_.CODE)
        );

        Page<Course> courses = courseRepo.findAll(specification, PageRequest.of(pageIndex - 1, pageSize));

        List<CourseDto> courseDtos = courses.getContent().stream().map(course1 ->
                CourseDto.builder().course(course1).build()).collect(Collectors.toList());

        return PageDto.of(courses, courseDtos);
    }

    @Override
    public CourseDto findOneById(Long id) {

        Course course = courseRepo.findById(id).orElseThrow(() ->
                new CustomException("Không tìm thấy khóa học"));

        List<Class> classes = classRepo.findAllByCourseEntity_Id(course.getId());

        return CourseDto.builder().course(course).classes(classes).build();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void save(CourseDto courseDto) {

        if (courseRepo.existsByName(courseDto.getName())) {
            throw new CustomException("Đã tồn tại khóa học");
        }

        courseRepo.save(courseConverter.toCourse(courseDto));

    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void update(CourseDto courseDto) {

        Course course = courseDto.getId() != null ? courseRepo.findById(courseDto.getId()).orElseThrow(() ->
                new CustomException("Không tìm thấy khóa học")) : null;

        if (course != null && courseRepo.existsByName(courseDto.getName()) &&
                !course.getName().equals(courseDto.getName().trim()))
            throw new DuplicateClassException(courseDto.getId());

        courseRepo.save(courseConverter.toCourse(courseDto));

    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void delete(Long id) {

        Course course = id != null ? courseRepo.findById(id).orElseThrow(() ->
                new CustomException("Không tìm thấy khóa học")) : null;

        if (classRepo.existsByCourseEntity_Id(id)) {
            throw new CustomException("Không thể xóa khóa học đã tồn tại lớp ");
        }

        courseRepo.delete(course);

    }
}

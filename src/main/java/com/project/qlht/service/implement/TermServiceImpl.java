package com.project.qlht.service.implement;

import com.project.qlht.constant.PageDto;
import com.project.qlht.constant.Subject_;
import com.project.qlht.converter.TermConverter;
import com.project.qlht.dto.TermDto;
import com.project.qlht.entity.Term;
import com.project.qlht.exception.CustomException;
import com.project.qlht.repository.SubjectRepo;
import com.project.qlht.repository.TermRepo;
import com.project.qlht.service.TermService;
import com.project.qlht.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TermServiceImpl implements TermService {

    private final TermRepo termRepo;
    private final TermConverter termConverter;
    private final SubjectRepo subjectRepo;

    public TermServiceImpl(TermRepo termRepo, TermConverter termConverter, SubjectRepo subjectRepo) {
        this.termRepo = termRepo;
        this.termConverter = termConverter;
        this.subjectRepo = subjectRepo;
    }

    @Override
    public PageDto<TermDto> findAll(Integer pageIndex, Integer pageSize, String name) {

        Specification<Term> specification = (root, cq, cb) -> cb.and(
                QueryUtils.buildLikeFilter(root, cb, name, Subject_.NAME)
        );
        Page<Term> termPage = termRepo.findAll(specification, PageRequest.of(pageIndex - 1, pageSize));

        List<TermDto> termDtos = termPage.getContent().stream().map(term ->
                TermDto.builder().term(term).build()).collect(Collectors.toList());

        return PageDto.of(termPage, termDtos);
    }

    @Override
    public TermDto detail(Long id) {

        Term term = termRepo.findById(id).orElseThrow(() ->
                new CustomException("Không tìm thấy học phần"));

        return TermDto.builder().term(term).build();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(TermDto termDto) {

        termRepo.save(termConverter.toTerm(termDto));

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(TermDto termDto) {

        termRepo.save(termConverter.toTerm(termDto));

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {

        Term term = termRepo.findById(id).orElseThrow(() -> new CustomException("Không tìm thấy học phần"));

        if (subjectRepo.existsByTermEntity_Id(id)) {
            throw new CustomException("Không được xóa học phần đã tồn tại môn học");
        }

        termRepo.delete(term);

    }
}

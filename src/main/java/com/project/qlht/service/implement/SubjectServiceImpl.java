package com.project.qlht.service.implement;

import java.util.List;
import java.util.stream.Collectors;

import com.project.qlht.constant.PageDto;
import com.project.qlht.constant.Semester_;
import com.project.qlht.constant.Subject_;
import com.project.qlht.converter.SubjectConverter;
import com.project.qlht.entity.Course;
import com.project.qlht.entity.Semester;
import com.project.qlht.entity.Subject;
import com.project.qlht.entity.Term;
import com.project.qlht.exception.CustomException;
import com.project.qlht.exception.DuplicationSubjectException;
import com.project.qlht.exception.NotSubjectException;
import com.project.qlht.repository.*;
import com.project.qlht.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.project.qlht.dto.SubjectDto;
import com.project.qlht.service.SubjectService;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

@Service
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepo subjectRepo;
    private final SubjectConverter subjectConverter;
    private final SemesterRepo semesterRepo;
    private final RegisterRepo registerRepo;
    private final ClassscheduleRepo classscheduleRepo;
    private final TermRepo termRepo;

    public SubjectServiceImpl(SubjectRepo subjectRepo, SubjectConverter subjectConverter, SemesterRepo semesterRepo,
                              RegisterRepo registerRepo, ClassscheduleRepo classscheduleRepo, TermRepo termRepo) {
        this.subjectRepo = subjectRepo;
        this.subjectConverter = subjectConverter;
        this.semesterRepo = semesterRepo;
        this.registerRepo = registerRepo;
        this.classscheduleRepo = classscheduleRepo;
        this.termRepo = termRepo;
    }

    @Override
    public PageDto<SubjectDto> findAll(Integer pageIndex, Integer pageSize, String name, String code, Long course, Long semester) {


        Specification<Subject> specification = (root, cq, cb) -> {
            Join<Subject, Semester> subjectCourseJoin = root.join(Subject_.SEMESTER, JoinType.LEFT);
            return cb.and(
                    QueryUtils.buildLikeFilter(root, cb, name, Subject_.NAME),
                    QueryUtils.buildLikeFilter(root, cb, code, Subject_.CODE),
                    QueryUtils.buildEqFilter(root, cb, subjectCourseJoin.get(Semester_.ID), semester)
            );
        };

        Page<Subject> subjectEntityPage = subjectRepo.findAll(specification,
                PageRequest.of(pageIndex - 1, pageSize));

        List<SubjectDto> subjectDtos = subjectEntityPage.getContent().stream().map(subjectEntity ->
                SubjectDto.builder()
                        .subject(subjectEntity)
                        .semester(subjectEntity.getSemesterEntity())
                        .term(subjectEntity.getTermEntity())
                        .build()).collect(Collectors.toList());

        return PageDto.of(subjectEntityPage, subjectDtos);
    }

    @Override
    public SubjectDto detail(Long id) {

        Subject subjectEntity = subjectRepo.findById(id).orElseThrow(() ->
                new CustomException("Không tìm thấy môn học"));

        SubjectDto subjectDto = SubjectDto.builder().subject(subjectEntity)
                .semester(subjectEntity.getSemesterEntity()).term(subjectEntity.getTermEntity()).build();

        return subjectDto;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(SubjectDto subjectDto) {

        if (subjectRepo.existsByNameAndTerm(subjectDto.getName(), subjectDto.getTerm())) {
            throw new DuplicationSubjectException(subjectDto.getId());
        }

        Semester semester = subjectDto.getSemesterDto() != null ?
                semesterRepo.findById(subjectDto.getSemesterDto().getId()).orElse(null) : null;

        Term term = subjectDto.getTermDto() != null ?
                termRepo.findById(subjectDto.getTermDto().getId()).orElse(null) : null;

        Subject subject = subjectConverter.toSubject(subjectDto);
        subject.setSemesterEntity(semester);
        subject.setTermEntity(term);

        subjectRepo.save(subject);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SubjectDto subjectDto) {
        Subject subjectEntity = subjectDto.getId() != null ?
                subjectRepo.findById(subjectDto.getId()).orElseThrow(() ->
                        new NotSubjectException(subjectDto.getId())) : null;

        if (subjectEntity != null && subjectRepo.existsByNameAndTerm(subjectDto.getName(), subjectDto.getTerm())
                && !subjectEntity.getName().equals(subjectDto.getName().trim())
                && !subjectEntity.getTerm().equals(subjectDto.getTerm()))
            throw new DuplicationSubjectException(subjectDto.getId());

        Semester semester = subjectDto.getSemesterDto() != null ?
                semesterRepo.findById(subjectDto.getSemesterDto().getId()).orElse(null) : null;

        Term term = subjectDto.getTermDto() != null ?
                termRepo.findById(subjectDto.getTermDto().getId()).orElse(null) : null;

        Subject subject = subjectConverter.toSubject(subjectDto);
        subject.setSemesterEntity(semester);
        subject.setTermEntity(term);

        subjectRepo.save(subject);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {

        Subject subjectEntity = subjectRepo.findById(id).orElseThrow(() -> new NotSubjectException(id));

        if (registerRepo.existsBySubjectEntity_Id(id)) {
            throw new CustomException("Không thể xóa môn học đã được đăng ký");
        }
        if (classscheduleRepo.existsBySubjectClassSchedule(subjectEntity)) {
            throw new CustomException("Không thể xóa môn học đã có lịch học");
        }

        subjectRepo.delete(subjectEntity);

    }
}

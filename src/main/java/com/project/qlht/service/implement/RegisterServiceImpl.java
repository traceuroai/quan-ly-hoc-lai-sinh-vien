package com.project.qlht.service.implement;

import com.project.qlht.constant.*;
import com.project.qlht.converter.RegisterConverter;
import com.project.qlht.entity.Register;
import com.project.qlht.entity.Subject;
import com.project.qlht.entity.User;
import com.project.qlht.exception.CustomException;
import com.project.qlht.repository.RegisterRepo;
import com.project.qlht.repository.SubjectRepo;
import com.project.qlht.repository.UserRepo;
import com.project.qlht.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.project.qlht.dto.RegisterDto;
import com.project.qlht.service.RegisterService;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RegisterServiceImpl implements RegisterService{

    private final RegisterRepo registerRepo;
    private final RegisterConverter registerConverter;
    private final UserRepo userRepo;
    private final SubjectRepo subjectRepo;

    public RegisterServiceImpl(RegisterRepo registerRepo, RegisterConverter registerConverter, UserRepo userRepo, SubjectRepo subjectRepo) {
        this.registerRepo = registerRepo;
        this.registerConverter = registerConverter;
        this.userRepo = userRepo;
        this.subjectRepo = subjectRepo;
    }

    @Override
    public PageDto<RegisterDto> findAll(Integer pageIndex, Integer pageSize, Long subjectId, Long userId) {

        Specification<Register> specification = (root, cq, cb) -> {
            Join<Register, User> registerUserJoin = root.join(Register_.USER, JoinType.LEFT);
            Join<Register, Subject> registerSubjectJoin = root.join(Register_.SUBJECT, JoinType.LEFT);

            return cb.and(
                    QueryUtils.buildEqFilter(root, cb, registerUserJoin.get(User_.ID), userId),
                    QueryUtils.buildEqFilter(root, cb, registerSubjectJoin.get(User_.ID), subjectId)
            );
        };

        Page<Register> registerPage = registerRepo.findAll(specification, PageRequest.of(pageIndex - 1, pageSize));

        List<RegisterDto> registerDtos = registerPage.getContent().stream().map(register ->
                RegisterDto.builder()
                        .subject(register.getSubjectEntity())
                        .user(register.getUserEntity()).build()).
                collect(Collectors.toList());

        return PageDto.of(registerPage, registerDtos);
    }

    @Override
    public RegisterDto detail(Long id) {

        Register register = registerRepo.findById(id).orElseThrow(() ->
                new CustomException("Không tìm thấy thông tin đăng ký"));

        return RegisterDto.builder().register(register)
                .user(register.getUserEntity())
                .subject(register.getSubjectEntity()).build();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(RegisterDto registerDto) {

        if (registerRepo.existsByUserEntity_IdAndSubjectEntity_Id(registerDto.getIdUser(), registerDto.getIdSubject())) {
            throw new CustomException("Đã tồn tại thông tin đăng ký");
        }
        registerRepo.save(registerConverter.toRegister(registerDto));

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(RegisterDto registerDto) {

        Register register = registerRepo.findById(registerDto.getId()).orElseThrow(() ->
                new CustomException("Không tồn tại thông tin đăng ký"));

        User user = userRepo.findById(registerDto.getIdUser()).orElseThrow( () -> new CustomException("Không tồn tại sinh viên "));

        Subject subject = subjectRepo.findById(registerDto.getIdSubject()).orElseThrow(() -> new CustomException("Không tồn tại môn học"));

        register.setUserEntity(user);
        register.setSubjectEntity(subject);
        registerRepo.save(register);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {

        Register register = registerRepo.findById(id).orElseThrow(() -> new CustomException("Không tồn tại thông tin đăng ký"));

        registerRepo.delete(register);

    }
}

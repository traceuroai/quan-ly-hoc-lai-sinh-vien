package com.project.qlht.service.implement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.project.qlht.config.JwtTokenProvider;
import com.project.qlht.constant.PageDto;
import com.project.qlht.constant.StatusContant_;
import com.project.qlht.constant.User_;
import com.project.qlht.converter.UserConverter;
import com.project.qlht.dto.CustomUserDetails;
import com.project.qlht.dto.LoginRequest;
import com.project.qlht.dto.detail.UserDetailDto;
import com.project.qlht.entity.RoleEntity;
import com.project.qlht.exception.CustomException;
import com.project.qlht.utils.QueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.project.qlht.dto.UserDto;
import com.project.qlht.entity.User;
import com.project.qlht.repository.UserRepo;
import com.project.qlht.service.UserService;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final UserConverter userConverter;

    public UserServiceImpl(UserRepo userRepo, JwtTokenProvider jwtTokenProvider,
                           AuthenticationManager authenticationManager, UserConverter userConverter) {
        this.userRepo = userRepo;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.userConverter = userConverter;
    }

    @Override
    public List<UserDto> findAll() {
        return userRepo.findAll().stream().map(userEntity -> UserDto.builder()
                .user(userEntity).build()).collect(Collectors.toList());
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepo.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserDetails(user);
    }


    @Transactional
    public UserDetails loadUserById(Long id) {
        User userEntity = userRepo.findOneById(id);
        if (userEntity == null) {
            throw new UsernameNotFoundException(id.toString());
        }
        return new CustomUserDetails(userEntity);
    }

    @Override
    public UserDto login(LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUserName(),
                        loginRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());

        User userEntity = userRepo.findOneById(jwtTokenProvider.getUserIdFromJWT(jwt));

        UserDto userDto = UserDto.builder().user(userEntity).build();
        userDto.setToken(jwt);
        return userDto;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public UserDto add(UserDetailDto userDetailDto) {

        User userEntity = userConverter.toUser(userDetailDto);
        userRepo.save(userEntity);
        return null;
    }

    @Override
    public UserDetails update(UserDetailDto userDetailDto) {

        return null;
    }


    @Override
    @Transactional(rollbackOn = Exception.class)
    public void delete(Long id) {

        User user = userRepo.findById(id).orElseThrow(() -> new CustomException("NotFound"));
        user.setStatus(0);
        userRepo.save(user);

    }

    @Override
    public PageDto<UserDto> findTeacher(Integer pageIndex, Integer pageSize, String name) {

        Specification<User> specification = (root, cq, cb) -> {
            Join<User, RoleEntity> userRoleEntityJoin = root.join(User_.ROLE, JoinType.LEFT);

            return cb.and(
                    QueryUtils.buildEqFilter(root, cb, userRoleEntityJoin.get(User_.ID), StatusContant_.GIAO_VIEN),
                    QueryUtils.buildLikeFilter(root, cb, name, User_.FULL_NAME)
            );
        };

        Page<User> userPage = userRepo.findAll(specification,
                PageRequest.of(pageIndex - 1, pageSize));

        List<UserDto> userDtos = userPage != null && !userPage.isEmpty() ? userPage.getContent().stream().map(user ->
                UserDto.builder().user(user).build()).collect(Collectors.toList()) : new ArrayList<>();

        return PageDto.of(userPage, userDtos);
    }

    @Override
    public UserDetailDto detailTeacher(Long teacherId) {

        User user = userRepo.findById(teacherId).orElseThrow(() -> new CustomException("Không tìm thấy giáo viên"));
        UserDetailDto userDetailDto = UserDetailDto.builder().user(user).build();

        return userDetailDto;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public UserDto addTeacher(UserDetailDto userDetailDto) {

        if (userRepo.existsByUsernameAndPhone(userDetailDto.getUserName(), userDetailDto.getPhone())) {
            throw new CustomException("Tài khoản đã tồn tại");
        }

        if (userRepo.existsByFullNameAndPhoneAndRole_Id(userDetailDto.getFullName(), userDetailDto.getPhone(), Long.valueOf(1))) {
            throw new CustomException("Giáo viên đã tồn tại");
        }

        User user = userConverter.toUser(userDetailDto);
        user.setStatus(1);
        user.setRole(new RoleEntity(StatusContant_.GIAO_VIEN));
        user = userRepo.save(user);

        UserDto userDto = UserDto.builder().user(user).build();

        return userDto;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public UserDto updateTeacher(UserDetailDto userDetailDto) {

        User user = userRepo.findById(userDetailDto.getId()).orElseThrow(() -> new CustomException("giáo viên không tồn tại"));

        user.setFullName(userDetailDto.getFullName());
        user.setPhone(userDetailDto.getPhone());
        user.setAddress(userDetailDto.getAddress());
        user.setSex(userDetailDto.getSex());
        user.setDateOfBirth(userDetailDto.getDateOfBirth());
        user = userRepo.save(user);

        UserDto userDto = UserDto.builder().user(user).build();

        return userDto;
    }

    @Override
    public PageDto<UserDto> findStudent(Integer pageIndex, Integer pageSize, String name) {
        Specification<User> specification = (root, cq, cb) -> {
            Join<User, RoleEntity> userRoleEntityJoin = root.join(User_.ROLE, JoinType.LEFT);

            return cb.and(
                    QueryUtils.buildEqFilter(root, cb, userRoleEntityJoin.get(User_.ID), StatusContant_.SINH_VIEN),
                    QueryUtils.buildLikeFilter(root, cb, name, User_.FULL_NAME)
            );
        };

        Page<User> userPage = userRepo.findAll(specification, PageRequest.of(pageIndex - 1, pageSize));

        List<UserDto> userDtos = userPage != null && !userPage.isEmpty() ? userPage.getContent().stream().map(user ->
                UserDto.builder().user(user).build()).collect(Collectors.toList()) : new ArrayList<>();

        return PageDto.of(userPage, userDtos);
    }

    @Override
    public UserDetailDto detailStudent(Long teacherId) {
        User user = userRepo.findById(teacherId).orElseThrow(() -> new CustomException("Không tìm thấy giáo viên"));
        UserDetailDto userDetailDto = UserDetailDto.builder().user(user).build();

        return userDetailDto;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public UserDto addStudent(UserDetailDto userDetailDto) {
        if (userRepo.existsByUsernameAndPhone(userDetailDto.getUserName(), userDetailDto.getPhone())) {
            throw new CustomException("Tài khoản đã tồn tại");
        }

        if (userRepo.existsByFullNameAndPhoneAndRole_Id(userDetailDto.getFullName(), userDetailDto.getPhone(), StatusContant_.SINH_VIEN)) {
            throw new CustomException("Sinh viên đã tồn tại");
        }

        User user = userConverter.toUser(userDetailDto);
        user.setStatus(1);
        user.setRole(new RoleEntity(StatusContant_.SINH_VIEN));
        user = userRepo.save(user);

        UserDto userDto = UserDto.builder().user(user).build();

        return userDto;
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public UserDto updateStudent(UserDetailDto userDetailDto) {
        User user = userRepo.findById(userDetailDto.getId()).orElseThrow(() -> new CustomException("giáo viên không tồn tại"));

        user.setFullName(userDetailDto.getFullName());
        user.setPhone(userDetailDto.getPhone());
        user.setAddress(userDetailDto.getAddress());
        user.setSex(userDetailDto.getSex());
        user.setDateOfBirth(userDetailDto.getDateOfBirth());
        user = userRepo.save(user);

        UserDto userDto = UserDto.builder().user(user).build();

        return userDto;
    }

}

package com.project.qlht.service.implement;

import java.util.List;

import org.springframework.stereotype.Service;

import com.project.qlht.dto.StudytimeDto;
import com.project.qlht.service.StudytimeService;

@Service
public class StudytimeServiceImpl implements StudytimeService{

	@Override
	public List<StudytimeDto> findAll() {
		return null;
	}

	@Override
	public StudytimeDto findOneById(Long id) {
		return null;
	}

	@Override
	public StudytimeDto save(StudytimeDto studytimeDto) {
		return null;
	}

	@Override
	public StudytimeDto update(StudytimeDto studytimeDto) {
		return null;
	}

	@Override
	public String delete(Long id) {
		return null;
	}
}

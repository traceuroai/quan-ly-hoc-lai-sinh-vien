package com.project.qlht.service.implement;

import java.util.List;

import org.springframework.stereotype.Service;

import com.project.qlht.dto.RoleDto;
import com.project.qlht.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService{

	@Override
	public List<RoleDto> findAll() {
		return null;
	}

	@Override
	public RoleDto findOneById(Long id) {
		return null;
	}

	@Override
	public RoleDto save(RoleDto roleDto) {
		return null;
	}

	@Override
	public RoleDto update(RoleDto roleDto) {
		return null;
	}

	@Override
	public String delete(Long id) {
		return null;
	}
}

package com.project.qlht.service.implement;

import com.project.qlht.constant.Address_;
import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.SemesterDto;
import com.project.qlht.entity.Semester;
import com.project.qlht.entity.Subject;
import com.project.qlht.exception.CustomException;
import com.project.qlht.repository.SemesterRepo;
import com.project.qlht.repository.SubjectRepo;
import com.project.qlht.service.SemesterService;
import com.project.qlht.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SemesterServiceImpl implements SemesterService {

    private final SemesterRepo semesterRepo;
    private final SubjectRepo subjectRepo;

    public SemesterServiceImpl(SemesterRepo semesterRepo, SubjectRepo subjectRepo) {
        this.semesterRepo = semesterRepo;
        this.subjectRepo = subjectRepo;
    }

    @Override
    public PageDto<SemesterDto> findAll(Integer pageIndex, Integer pageSize, String userName, String nameAddress) {
        Specification<Semester> specification = (root, cq, cb) -> cb.and(
                QueryUtils.buildLikeFilter(root, cb, nameAddress, Address_.NAME)
        );

        Page<Semester> semesterPage = semesterRepo.findAll(specification,
                PageRequest.of(pageIndex - 1, pageSize));

        List<SemesterDto> semesterDtos = semesterPage.getContent().stream().map(semester ->
                SemesterDto.builder().semester(semester).subject(semester.getSubjects()).build()).collect(Collectors.toList());

        return PageDto.of(semesterPage, semesterDtos);
    }

    @Override
    public SemesterDto findOneById(Long id) {
        Semester semester = semesterRepo.findById(id).orElseThrow(() ->
                new CustomException("Không tồn tại kỳ học"));

        return SemesterDto.builder().semester(semester).subject(semester.getSubjects()).build();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(SemesterDto semesterDto) {

        if (semesterRepo.existsByName(semesterDto.getName())) {
            throw new CustomException("Kỳ học đã tồn tại");
        }

        Semester semester = new Semester();
        semester.setName(semesterDto.getName());

        semesterRepo.save(semester);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(SemesterDto semesterDto) {

        Semester semester = semesterDto.getId() != null ? semesterRepo.findById(semesterDto.getId()).orElseThrow(() ->
                new CustomException("Không tồn tại kỳ học")) : null;

        if (semester != null && semesterRepo.existsByName(semesterDto.getName()) &&
                !semester.getName().equals(semesterDto.getName().trim()))
            throw new CustomException("Kỳ học đã tồn tại");

        semester.setName(semesterDto.getName());

        semesterRepo.save(semester);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {

        Semester semester = id != null ? semesterRepo.findById(id).orElseThrow(() ->
                new CustomException("Không tồn tại kỳ học")) : null;

        if (subjectRepo.existsBySemesterEntity_Id(id)) {
            throw new CustomException("Không thể xóa kỳ học tồn tại môn học");
        }

        semesterRepo.delete(semester);

    }
}

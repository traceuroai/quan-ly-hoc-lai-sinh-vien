package com.project.qlht.service.implement;

import java.util.List;
import java.util.stream.Collectors;

import com.project.qlht.constant.Class_;
import com.project.qlht.constant.Course_;
import com.project.qlht.constant.PageDto;
import com.project.qlht.converter.ClassConverter;
import com.project.qlht.entity.Class;
import com.project.qlht.entity.Course;
import com.project.qlht.exception.CustomException;
import com.project.qlht.exception.DuplicateClassException;
import com.project.qlht.exception.NotClassException;
import com.project.qlht.repository.ClassRepo;
import com.project.qlht.repository.CourseRepo;
import com.project.qlht.repository.UserRepo;
import com.project.qlht.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.project.qlht.dto.ClassDto;
import com.project.qlht.service.ClassService;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;

@Service
public class ClassServiceImpl implements ClassService {

    private final ClassRepo classRepo;
    private final ClassConverter classConverter;
    private final CourseRepo courseRepo;
    private final UserRepo userRepo;

    public ClassServiceImpl(ClassRepo classRepo, ClassConverter classConverter, CourseRepo courseRepo, UserRepo userRepo) {
        this.classRepo = classRepo;
        this.classConverter = classConverter;
        this.courseRepo = courseRepo;
        this.userRepo = userRepo;
    }

    @Override
    public PageDto<ClassDto> findAll(Integer pageIndex, Integer pageSize, String name, Integer course) {

        Specification<Class> specification = (root, cq, cb) -> {
            Join<Class, Course> classCourseJoin = root.join(Class_.COURSE_ENTITY, JoinType.LEFT);
            return cb.and(
                    QueryUtils.buildLikeFilter(root, cb, name, Class_.NAME),
                    QueryUtils.buildLikeFilter(root, cb, name, Class_.CODE),
                    QueryUtils.buildEqFilter(root, cb, classCourseJoin.get(Course_.ID), course)
            );
        };

        Page<Class> classEntityPage = classRepo.findAll(specification,
                PageRequest.of(pageIndex - 1, pageSize));

        List<ClassDto> classDtos = classEntityPage.getContent().stream().map(classEntity ->
                ClassDto.builder().classes(classEntity).course(classEntity.getCourseEntity())
                        .build()).collect(Collectors.toList());

        return PageDto.of(classEntityPage, classDtos);

    }

    @Override
    public ClassDto findOneById(Long id) {

        Class classEntity = classRepo.findById(id).orElseThrow(() -> new NotClassException(id));

        return ClassDto.builder().classes(classEntity)
                .course(classEntity.getCourseEntity())
                .user(classEntity.getUserEntities())
                .build();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(ClassDto classDto) {

        if (classRepo.existsByNameAndCourse(classDto.getName(), classDto.getCourse())) {
            throw new DuplicateClassException(classDto.getId());
        }
        Course course = null;
        if (classDto.getCourseDto() != null) {
            course = courseRepo.findById(classDto.getCourseDto().getId()).orElseThrow(() ->
                    new CustomException("Không tìm thấy khóa học"));
        }

        Class classes = classConverter.toClass(classDto);
        classes.setCourseEntity(course);

        classRepo.save(classes);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(ClassDto classDto) {

        Class classEntity = classDto.getId() != null ? classRepo.findById(classDto.getId()).orElseThrow(() ->
                new NotClassException(classDto.getId())) : null;

        Course course = null;
        if (classDto.getCourseDto() != null) {
            course = courseRepo.findById(classDto.getCourseDto().getId()).orElseThrow(() ->
                    new CustomException("Không tìm thấy khóa học"));
        }

        if (classEntity != null && classRepo.existsByNameAndCourse(classDto.getName(), classDto.getCourse()) &&
                !classEntity.getName().equals(classDto.getName().trim()) && !classEntity.getCourse().equals(classDto.getCourse()))
            throw new DuplicateClassException(classDto.getId());

        Class classes = classConverter.toClass(classDto);
        classes.setCourseEntity(course);

        classRepo.save(classes);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Long id) {

        Class classEntity = classRepo.findById(id)
                .orElseThrow(() -> new NotClassException(id));

        if (userRepo.existsByClassEntity_Id(id)) {
            throw new CustomException("Không thể xóa lớp học tồn tại sinh viên !");
        }

        classRepo.delete(classEntity);
    }
}

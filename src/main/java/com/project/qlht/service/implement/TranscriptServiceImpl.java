package com.project.qlht.service.implement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.project.qlht.converter.TranscriptDetailConverter;
import com.project.qlht.dto.detail.TranscriptDetailDto;
import com.project.qlht.entity.*;
import com.project.qlht.exception.NotSubjectException;
import com.project.qlht.exception.NotTranscriptException;
import com.project.qlht.exception.NotUserException;
import com.project.qlht.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.project.qlht.dto.TranscriptDto;
import com.project.qlht.service.TranscriptService;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TranscriptServiceImpl implements TranscriptService {

    @Autowired
    private SubjectRepo subjectRepo;
    @Autowired
    private TranscriptRepo transcriptRepo;
    @Autowired
    private TranscriptDetailRepo transcriptDetailRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private TranscriptDetailConverter transcriptDetailConverter;
    @Autowired
    private RegisterRepo registerRepo;
    @Autowired
    private AddressRepo addressRepo;

    @Override
    public List<TranscriptDto> findAll() {

        List<Transcript> transcriptEntityList = transcriptRepo.findAll();

        return transcriptEntityList.stream().map(transcriptEntity -> TranscriptDto.builder()
                .transcript(transcriptEntity)
                .build()).collect(Collectors.toList());
    }

    @Override
    public TranscriptDto detail(long id) {

        Transcript transcriptEntity = transcriptRepo
                .findById(id).orElseThrow(() -> new NotTranscriptException(id));

        List<TranscriptDetailEntity> transcriptDetailEntities = transcriptDetailRepo
                .findAllByTranscript(transcriptEntity.getId());

        User userEntity = userRepo.findOneById(transcriptEntity.getTranscriptTeacher().getId());

        return TranscriptDto.builder().transcript(transcriptEntity)
                .transcriptDetail(transcriptDetailEntities).teacher(userEntity).build();
    }

    @Override
    @Transactional
    public TranscriptDto create(Long subjectId, Long userId) {

        Subject subjectEntity = subjectRepo.findById(subjectId)
                .orElseThrow(() -> new NotSubjectException(subjectId));

        User teacher = userRepo.findOneById(userId);

        Transcript transcriptEntity = new Transcript();
        transcriptEntity.setSubjectTranscript(subjectEntity);
        transcriptEntity.setTranscriptTeacher(teacher);
        transcriptEntity = transcriptRepo.save(transcriptEntity);

        List<User> users = registerRepo.findAllUser(transcriptEntity.getSubjectTranscript().getId());
        List<TranscriptDetailEntity> transcriptDetails = new ArrayList<>();
        if(!users.isEmpty() && transcriptEntity != null)
        {
            for(User userEntity : users)
            {
                TranscriptDetailEntity transcriptDetail = new TranscriptDetailEntity();
                transcriptDetail.setUser(userEntity);
                transcriptDetail.setFullName(userEntity.getFullName());
                transcriptDetail.setDateOfBirth(userEntity.getDateOfBirth());
                transcriptDetail.setTranscript(transcriptEntity);
                transcriptDetails.add(transcriptDetailRepo.save(transcriptDetail));
            }
        }

        return TranscriptDto.builder().transcript(transcriptEntity)
                .teacher(teacher).subject(subjectEntity)
                .transcriptDetail(transcriptDetails).build();
    }

    @Override
    @Transactional
    public List<TranscriptDetailDto> update(List<TranscriptDetailDto> transcripts) {

        Transcript transcriptEntity = transcriptRepo.findById(transcripts.get(0).getTranscriptId())
                .orElseThrow(() -> new NotTranscriptException(transcripts.get(0).getTranscriptId()));

        for (TranscriptDetailDto transcriptDetailDto : transcripts)
        {
            TranscriptDetailEntity transcriptDetailEntity = transcriptDetailConverter.toTranscript(transcriptDetailDto);
            User userEntity = userRepo.findById(transcriptDetailDto.getUser().getId())
                    .orElseThrow(() -> new NotUserException(transcriptDetailDto.getUser().getId()));
            transcriptDetailEntity.setId(transcriptDetailDto.getId());
            transcriptDetailEntity.setDateOfBirth(userEntity.getDateOfBirth());
            transcriptDetailEntity.setTranscript(transcriptEntity);
            transcriptDetailRepo.save(transcriptDetailEntity);
        }
        return null;
    }

    @Override
    public TranscriptDetailDto search(Long userId, Long subjectId) {

        Subject subjectEntity = subjectRepo.findById(subjectId)
                .orElseThrow(() -> new NotSubjectException(subjectId));
        User userEntity = userRepo.findById(userId).orElseThrow(() -> new NotUserException(userId));
        Transcript transcriptEntity = transcriptRepo.findBySubjectTranscript(subjectEntity.getId());
        TranscriptDetailEntity transcriptDetailEntity = transcriptDetailRepo.searchTranscript(transcriptEntity.getId(), userId);
        return transcriptDetailEntity != null ? TranscriptDetailDto.builder().transcript(transcriptDetailEntity).build() : null;
    }

    @Override
    public String delete(Long id) {
        return null;
    }
}

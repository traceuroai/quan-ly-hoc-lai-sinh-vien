package com.project.qlht.service.implement;

import com.project.qlht.constant.ClassSchedule_;
import com.project.qlht.constant.PageDto;
import com.project.qlht.constant.Subject_;
import com.project.qlht.constant.User_;
import com.project.qlht.converter.ClassscheduleConverter;
import com.project.qlht.converter.StudytimeConverter;
import com.project.qlht.dto.ClassScheduleDto;
import com.project.qlht.dto.StudytimeDto;
import com.project.qlht.dto.detail.ClassScheduleDetailDto;
import com.project.qlht.entity.*;
import com.project.qlht.exception.*;
import com.project.qlht.repository.*;
import com.project.qlht.service.ClassscheduleService;
import com.project.qlht.utils.QueryUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Join;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ClassscheduleServiceImpl implements ClassscheduleService {

    private final ClassscheduleRepo classscheduleRepo;
    private final ClassscheduleConverter classscheduleConverter;
    private final StudytimeRepo studytimeRepo;
    private final StudytimeConverter studytimeConverter;
    private final SubjectRepo subjectRepo;
    private final AddressRepo addressRepo;
    private final UserRepo userRepo;
    private final TranscriptRepo transcriptRepo;
    private final RegisterRepo registerRepo;
    private final TranscriptDetailRepo transcriptDetailRepo;

    public ClassscheduleServiceImpl(ClassscheduleRepo classscheduleRepo,
                                    ClassscheduleConverter classscheduleConverter,
                                    StudytimeRepo studytimeRepo,
                                    StudytimeConverter studytimeConverter,
                                    SubjectRepo subjectRepo,
                                    AddressRepo addressRepo,
                                    UserRepo userRepo,
                                    TranscriptRepo transcriptRepo,
                                    RegisterRepo registerRepo,
                                    TranscriptDetailRepo transcriptDetailRepo) {
        this.classscheduleRepo = classscheduleRepo;
        this.classscheduleConverter = classscheduleConverter;
        this.studytimeRepo = studytimeRepo;
        this.studytimeConverter = studytimeConverter;
        this.subjectRepo = subjectRepo;
        this.addressRepo = addressRepo;
        this.userRepo = userRepo;
        this.transcriptRepo = transcriptRepo;
        this.registerRepo = registerRepo;
        this.transcriptDetailRepo = transcriptDetailRepo;
    }

    @Override
    public PageDto<ClassScheduleDto> findAll(Long subjectId, Long teacherId, Integer pageIndex, Integer pageSize) {

        Specification<ClassSchedule> specification = (root, cq, cb) -> {
            Join<ClassSchedule, Subject> classScheduleEntitySubjectEntityJoin =
                    root.join(ClassSchedule_.SUBJECT);
            Join<ClassSchedule, User> classScheduleEntityUserEntityJoin =
                    root.join(ClassSchedule_.TEACHER);

            return cb.and(
                    QueryUtils.buildEqFilter(root, cb, classScheduleEntityUserEntityJoin.get(User_.ID), teacherId),
                    QueryUtils.buildEqFilter(root, cb, classScheduleEntitySubjectEntityJoin.get(Subject_.ID), subjectId)
            );
        };

        Page<ClassSchedule> classScheduleEntityPage = classscheduleRepo
                .findAll(specification, PageRequest.of(0, 10));

        List<ClassScheduleDto> classScheduleDtos = classScheduleEntityPage
                .stream().map(classscheduleEntity -> ClassScheduleDto.builder()
                        .classSchedule(classscheduleEntity).build()).collect(Collectors.toList());

        return PageDto.of(classScheduleEntityPage, classScheduleDtos);
    }

    @Override
    public ClassScheduleDetailDto detail(Long id) {

        ClassSchedule classscheduleEntity = classscheduleRepo.findById(id)
                .orElseThrow(() -> new NotClassScheduleException(id));

        return ClassScheduleDetailDto.builder().classSchedule(classscheduleEntity)
                .subject(classscheduleEntity.getSubjectClassSchedule())
                .teacher(classscheduleEntity.getUserClassSchedule())
                .address(classscheduleEntity.getAddressEntity())
                .studyTime(classscheduleEntity.getStudytimeEntities()).build();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void add(ClassScheduleDetailDto classScheduleDetailDto) {

        subjectRepo.findById(classScheduleDetailDto.getSubjectDto().getId())
                .orElseThrow(() -> new NotSubjectException(classScheduleDetailDto.getSubjectDto().getId()));

        if (classScheduleDetailDto.getAddress() != null) {
            addressRepo.findById(classScheduleDetailDto.getAddress().getId())
                    .orElseThrow(() -> new NotAddressExistException(classScheduleDetailDto.getAddress().getId()));
        }
        if (classScheduleDetailDto.getStudytime() != null && !classScheduleDetailDto.getStudytime().isEmpty()
                && classScheduleDetailDto.getStudytime().size() != classScheduleDetailDto.getLessonNumber()) {
            throw new StudyTimeException(classScheduleDetailDto.getLessonNumber());
        }

        ClassSchedule classscheduleEntity = classscheduleRepo
                .save(classscheduleConverter.toClassSchedule(classScheduleDetailDto));

        List<Studytime> studytimeEntities = new ArrayList<>();
        if (classScheduleDetailDto.getStudytime() != null && !classScheduleDetailDto.getStudytime().isEmpty()) {
            for (StudytimeDto studytimeDto : classScheduleDetailDto.getStudytime()) {
                Studytime studytimeEntity = studytimeConverter.toStudyTime(studytimeDto);
                studytimeEntity.setClassSchedule(classscheduleEntity);
                studytimeEntities.add(studytimeEntity);
            }
        }
        studytimeRepo.saveAll(studytimeEntities);
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void update(ClassScheduleDetailDto classScheduleDetailDto) {

        classscheduleRepo.findById(classScheduleDetailDto.getId())
                .orElseThrow(() -> new NotClassScheduleException(classScheduleDetailDto.getId()));

        if (classScheduleDetailDto.getSubjectDto() != null) {
            subjectRepo.findById(classScheduleDetailDto.getSubjectDto().getId())
                    .orElseThrow(() -> new NotSubjectException(classScheduleDetailDto.getSubjectDto().getId()));
        }
        if (classScheduleDetailDto.getAddress() != null) {
            addressRepo.findById(classScheduleDetailDto.getAddress().getId())
                    .orElseThrow(() -> new NotAddressExistException(classScheduleDetailDto.getAddress().getId()));
        }
        if (classScheduleDetailDto.getStudytime() != null && !classScheduleDetailDto.getStudytime().isEmpty()
                && classScheduleDetailDto.getStudytime().size() != classScheduleDetailDto.getLessonNumber()) {
            throw new StudyTimeException(classScheduleDetailDto.getLessonNumber());
        }

        Subject subjectEntity = subjectRepo.findOneById(classScheduleDetailDto.getSubjectDto().getId());

        ClassSchedule classScheduleEntity = classscheduleConverter.toClassSchedule(classScheduleDetailDto);
        classScheduleEntity.setSubjectClassSchedule(subjectEntity);
        classScheduleEntity = classscheduleRepo.save(classScheduleEntity);
        if (!classScheduleDetailDto.getStudytime().isEmpty()) {
            for (StudytimeDto studytimeDto : classScheduleDetailDto.getStudytime()) {
                Studytime studytimeEntity = studytimeConverter.toStudyTime(studytimeDto);
                studytimeEntity.setClassSchedule(classScheduleEntity);
                studytimeRepo.save(studytimeEntity);
            }
        }
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public void delete(Long id) {
        ClassSchedule classscheduleEntity = classscheduleRepo.findById(id)
                .orElseThrow(() -> new NotAddressExistException(id));
        studytimeRepo.deleteAllByClassSchedule(classscheduleEntity.getId());
        classscheduleRepo.delete(classscheduleEntity);
    }

    @Override
    public ClassScheduleDto search(Long subjectId) {
        Subject subjectEntity = subjectRepo.findById(subjectId)
                .orElseThrow(() -> new NotSubjectException(subjectId));
        ClassSchedule classscheduleEntity = classscheduleRepo.findBySubjectClassSchedule(subjectEntity);
        if (classscheduleEntity == null) {
            throw new NotClassScheduleException();
        }

        User userEntity = null;
        if (classscheduleEntity.getUserClassSchedule() != null) {
            userEntity = userRepo.findById(classscheduleEntity.getUserClassSchedule().getId()).orElse(null);
        }
        Address addressEntity = null;
        if (classscheduleEntity.getAddressEntity() != null) {
            addressEntity = addressRepo.findById(classscheduleEntity.getUserClassSchedule().getId()).orElse(null);
        }

        return ClassScheduleDto.builder().classSchedule(classscheduleEntity)
                .subject(subjectEntity).address(addressEntity).teacher(userEntity)
                .build();
    }

    @Override
    public ClassScheduleDto search2(Long subjectId, Long userId) {

        Subject subjectEntity = subjectRepo.findById(subjectId)
                .orElseThrow(() -> new NotSubjectException(subjectId));

        ClassSchedule classscheduleEntity = classscheduleRepo.findBySubjectClassSchedule(subjectEntity);
        if (classscheduleEntity == null) {
            throw new NotClassScheduleException();
        }
        if (classscheduleEntity.getUserClassSchedule() == null) {
            throw new NotUserException();
        }

        User userEntity = userRepo.findById(userId).orElseThrow(() -> new NotUserException(userId));

        if (!userEntity.getRole().getCode().equals("Giao-Vu") && userId != classscheduleEntity.getUserClassSchedule().getId()) {
            throw new NotUserException(userId);
        }

        Address addressEntity = null;
        if (classscheduleEntity.getAddressEntity() != null) {
            addressEntity = addressRepo.findById(classscheduleEntity.getUserClassSchedule().getId()).orElse(null);
        }

        return ClassScheduleDto.builder().classSchedule(classscheduleEntity)
                .subject(subjectEntity).address(addressEntity).teacher(userEntity)
                .build();

    }
}

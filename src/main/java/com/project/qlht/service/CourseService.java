package com.project.qlht.service;

import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.ClassDto;
import com.project.qlht.dto.CourseDto;


public interface CourseService {

    PageDto<CourseDto> findAll(Integer pageIndex, Integer pageSize, String name, String code);

    CourseDto findOneById(Long id);

    void save(CourseDto courseDto);

    void update(CourseDto courseDto);

    void delete(Long id);

}

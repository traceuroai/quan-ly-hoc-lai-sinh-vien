package com.project.qlht.service;

import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.SemesterDto;

public interface SemesterService {

    PageDto<SemesterDto> findAll(Integer pageIndex, Integer pageSize, String userName, String nameAddress);

    SemesterDto findOneById(Long id);

    void save(SemesterDto semesterDto);

    void update(SemesterDto semesterDto);

    void delete(Long id);

}

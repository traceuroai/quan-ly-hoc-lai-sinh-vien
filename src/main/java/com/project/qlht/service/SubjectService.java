package com.project.qlht.service;

import java.util.List;

import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.SubjectDto;
import org.springframework.data.domain.Page;

public interface SubjectService {
	
	PageDto<SubjectDto> findAll(Integer pageIndex, Integer pageSize, String name, String code, Long course, Long semester);

	SubjectDto detail(Long id);

	void save(SubjectDto subjectDto);

	void update(SubjectDto subjectDto);

	void delete(Long id);

}

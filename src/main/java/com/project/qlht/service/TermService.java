package com.project.qlht.service;

import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.TermDto;

public interface TermService {

    PageDto<TermDto> findAll(Integer pageIndex, Integer pageSize, String name);

    TermDto detail(Long id);

    void save(TermDto termDto);

    void update(TermDto termDto);

    void delete(Long id);

}

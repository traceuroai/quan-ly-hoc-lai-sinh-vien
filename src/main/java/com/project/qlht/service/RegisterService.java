package com.project.qlht.service;

import com.project.qlht.constant.PageDto;
import com.project.qlht.dto.RegisterDto;

public interface RegisterService {

	PageDto<RegisterDto> findAll(Integer pageIndex, Integer pageSize, Long subjectId, Long userId);

	RegisterDto detail(Long id);

	void save(RegisterDto registerDto);

	void update(RegisterDto registerDto);

	void delete(Long id);

}

package com.project.qlht;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication()
//@CrossOrigin
public class ProjectQlhtApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectQlhtApplication.class, args);
		
	}
}

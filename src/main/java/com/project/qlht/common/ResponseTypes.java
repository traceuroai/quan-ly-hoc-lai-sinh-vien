package com.project.qlht.common;

public class ResponseTypes {
    public static final ResponseError SUCCESS = new ResponseError(0, "SUCCESS");
    public static final ResponseError ERROR = new ResponseError(99, "ERROR");
    public static final ResponseError BAD_REQUEST = new ResponseError(1, "BAD_REQUEST");
    public static final ResponseError TOKEN_INVALID = new ResponseError(3, "TOKEN_INVALID");
    public static final ResponseError DUPLICATE_RESOURCE = new ResponseError(4, "DUPLICATE_RESOURCE");
}

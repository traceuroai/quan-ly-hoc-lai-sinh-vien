package com.project.qlht.common;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
public class UserJwt {

    private String token;

}

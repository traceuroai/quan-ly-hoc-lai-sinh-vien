package com.project.qlht.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "role")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleEntity{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)// auto_icrement
	private Long id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;
	
	@OneToMany(mappedBy = "role", cascade = CascadeType.REMOVE)
	private List<User> users = new ArrayList<>();

	public RoleEntity(Long id) {
		this.id = id;
	}
}

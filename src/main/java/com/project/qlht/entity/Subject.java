package com.project.qlht.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "subjects")
@Data
public class Subject extends Abstract {

	@Column(name = "code")
	private String code;

	@Column(name = "name")
	private String name;

	@Column(name = "credits")
	private int credits;

	@Column(name = "term")
	private String term;

	//todo add term_id in db
	@ManyToOne
	@JoinColumn(name = "term_id")
	private Term termEntity;

	@OneToMany(mappedBy = "subjectClassSchedule", cascade = CascadeType.REMOVE)
	private List<ClassSchedule> classscheduleEntities = new ArrayList<>();

	@OneToMany(mappedBy = "subjectTranscript", cascade = CascadeType.REMOVE)
	private List<Transcript> transcriptEntities = new ArrayList<>();

	@OneToMany(mappedBy = "subjectEntity", cascade = CascadeType.REMOVE)
	private List<Register> registers = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "semester_id")
	private Semester semesterEntity;

}

package com.project.qlht.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "address")
@Data
public class Address extends Abstract {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "address")
	private String address;
	
	@OneToMany(mappedBy = "addressEntity" )
	private List<ClassSchedule> classSchedules = new ArrayList<>();
}

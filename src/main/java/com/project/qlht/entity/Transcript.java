package com.project.qlht.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "transcript")
@Data
public class Transcript extends Abstract {

	@Column(name = "testday")
	private Date testDay;
	
	@ManyToOne
	@JoinColumn(name = "idsubject")
	private Subject subjectTranscript;
	
	@ManyToOne
	@JoinColumn(name = "idteacher")
	private User transcriptTeacher;

	@OneToMany(mappedBy = "transcript", cascade = CascadeType.REMOVE)
	private List<TranscriptDetailEntity> transcriptDetails = new ArrayList<>();
}

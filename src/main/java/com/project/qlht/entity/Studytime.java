package com.project.qlht.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "studytime")
@Data
public class Studytime extends Abstract {

	@Column(name = "sessions")
	private String sessions;
	
	@Column(name = "stuff")
	private String stuff;
	
	@Column(name = "days")
	private Date days;
	
	@Column(name = "times")
	private String times;
	
	@ManyToOne
	@JoinColumn(name = "idclassschedule")
	private ClassSchedule classSchedule;
	
}

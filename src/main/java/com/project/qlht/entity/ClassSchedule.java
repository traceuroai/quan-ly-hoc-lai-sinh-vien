package com.project.qlht.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "classschedule")
@Data
public class ClassSchedule extends Abstract {

	@Column(name = "term")
	private String term;
	
	@Column(name = "class")
	private String classes;
	
	@Column(name = "course")
	private String course;
	
	@Column(name = "amount")
	private int amount;
	
	@Column(name = "lessonNumber")
	private int lessonNumber;
	
	@Column(name = "note")
	private String note;
	
	@ManyToOne
	@JoinColumn(name = "idaddress")
	private Address addressEntity;
	
	@OneToMany(mappedBy = "classSchedule", cascade = CascadeType.REMOVE)
	private List<Studytime> studytimeEntities = new ArrayList<>();
	
	@ManyToOne
	@JoinColumn(name = "idsubject")
	private Subject subjectClassSchedule;
	
	@ManyToOne
	@JoinColumn(name = "idteacher")
	private User userClassSchedule;
	
}

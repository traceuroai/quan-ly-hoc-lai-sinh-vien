package com.project.qlht.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "user")
@Data
@Where(clause = "status = 1")
public class User extends Abstract {

	@Column(name = "username", nullable = false, unique = true)
	private String username;

	@Column(name = "fullname")
	private String fullName;

	@Column(name = "password")
	private String password;

	@Column(name = "phone")
	private String phone;

	@Column(name = "address")
	private String address;

	@Column(name = "sex")
	private String sex;

	@Column(name = "dateofbirth")
	private Date dateOfBirth;

	@Column(name = "status")
	private Integer status;

	@Column(name = "code")
	private String code;

	@ManyToOne
	@JoinColumn(name = "idclass")
	private Class classEntity;
	
	@ManyToOne
	@JoinColumn(name = "idrole")
	private RoleEntity role;

	@OneToMany(mappedBy = "userEntity", cascade = CascadeType.REMOVE)
	private List<Register> registers = new ArrayList<>();

	@OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
	private List<TranscriptDetailEntity> transcriptDetails = new ArrayList<>();

	@OneToMany(mappedBy = "transcriptTeacher", cascade = CascadeType.REMOVE)
	private List<Transcript> transcriptTeacher = new ArrayList<>();

	@OneToMany(mappedBy = "userClassSchedule", cascade = CascadeType.REMOVE)
	private List<ClassSchedule> classSchedules = new ArrayList<>();

}

package com.project.qlht.entity;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "register")
@Data
public class Register extends Abstract {

	@ManyToOne
	@JoinColumn(name = "iduser")
	private User userEntity;
	
	@ManyToOne
	@JoinColumn(name = "idsubject")
	private Subject subjectEntity;
	
}

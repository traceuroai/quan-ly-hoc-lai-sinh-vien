package com.project.qlht.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "transcriptdetail")
@Data
public class TranscriptDetailEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)// auto_icrement
    @Column(name = "id")
    private Long id;

    @Column(name = "fullname")
    private String fullName;

    @Column(name = "mark")
    private int mark;

    @Column(name = "dateofbirth")
    private Date dateOfBirth;

    @Column(name = "numberofsheets")
    private int numberOfSheets;

    @Column(name = "note")
    private String note;

    @ManyToOne
    @JoinColumn(name = "transcriptid")
    private Transcript transcript;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User user;

}

package com.project.qlht.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Entity
@Table(name = "class")
@Data
public class Class extends Abstract {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "code")
	private String code;
	
	@Column(name = "name")
	private String name;

	@Column(name = "course")
	private String course;

	@OneToMany(mappedBy = "classEntity", cascade = CascadeType.REMOVE)
	private List<User> userEntities = new ArrayList<>();

	@ManyToOne
	@JoinColumn(name = "id_course")
	private Course courseEntity;
}

package com.project.qlht.converter;

import org.springframework.stereotype.Component;

import com.project.qlht.dto.RoleDto;
import com.project.qlht.entity.RoleEntity;

@Component
public class RoleConverter {

    public RoleEntity toRole(RoleDto roleDto)
    {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(roleDto.getId());
        roleEntity.setCode(roleDto.getCode());
        roleEntity.setName(roleDto.getName());
        return roleEntity;
    }
	
}

package com.project.qlht.converter;

import com.project.qlht.dto.UserDto;
import com.project.qlht.entity.Subject;
import com.project.qlht.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.qlht.dto.RegisterDto;
import com.project.qlht.entity.Register;

@Component
public class RegisterConverter {

    @Autowired
    SubjectConverter subjectConverter;

    public Register toRegister(RegisterDto registerDto)
    {
        Register registerEntity = new Register();
        registerEntity.setId(registerDto.getId());
        registerEntity.setCreatedDate(registerDto.getCreatedDate());
        registerEntity.setModifiedDate(registerDto.getModifiedDate());
        registerEntity.setCreatedBy(registerDto.getCreatedBy());
        registerEntity.setModifiedBy(registerDto.getModifiedBy());
        registerEntity.setUserEntity(toUser(registerDto.getIdUser()));
        registerEntity.setSubjectEntity(toSubject(registerDto.getIdSubject()));
        return registerEntity;
    }

    public User toUser(Long id)
    {
        User userEntity = new User();
        userEntity.setId(id);
        return userEntity;
    }

    public Subject toSubject(Long id)
    {
        Subject subject = new Subject();
        subject.setId(id);
        return subject;
    }

}

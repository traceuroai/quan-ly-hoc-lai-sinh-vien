package com.project.qlht.converter;

import com.project.qlht.dto.TermDto;
import com.project.qlht.entity.Term;
import org.springframework.stereotype.Component;

@Component
public class TermConverter {

    public Term toTerm(TermDto termDto)
    {
        Term term = new Term();
        term.setId(termDto.getId());
        term.setCode(termDto.getCode());
        term.setName(termDto.getName());
        return term;
    }

}

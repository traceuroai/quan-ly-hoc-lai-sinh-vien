package com.project.qlht.converter;

import com.project.qlht.dto.UserDto;
import com.project.qlht.dto.detail.TranscriptDetailDto;
import com.project.qlht.entity.TranscriptDetailEntity;
import com.project.qlht.entity.User;
import org.springframework.stereotype.Component;

@Component
public class TranscriptDetailConverter {

    public TranscriptDetailEntity toTranscript(TranscriptDetailDto transcriptDetailDto)
    {
        TranscriptDetailEntity transcriptDetailEntity = new TranscriptDetailEntity();
        transcriptDetailEntity.setUser(toUser(transcriptDetailDto.getUser()));
        transcriptDetailEntity.setFullName(transcriptDetailDto.getFullName());
        transcriptDetailEntity.setDateOfBirth(transcriptDetailDto.getDateOfBirth());
        transcriptDetailEntity.setMark(transcriptDetailDto.getMark());
        transcriptDetailEntity.setNumberOfSheets(transcriptDetailDto.getNumberOfSheets());
        transcriptDetailEntity.setNote(transcriptDetailDto.getNote());
        return transcriptDetailEntity;
    }

    public User toUser(UserDto userDto)
    {
        User userEntity = new User();
        userEntity.setId(userDto.getId());
//        userEntity.setUsername(userDto.getUserName());
//        userEntity.setFullName(userDto.getFullName());
//        userEntity.setPassword(userDto.getPassword());
//        userEntity.setPhone(userDto.getPhone());
//        userEntity.setAddress(userDto.getAddress());
//        userEntity.setSex(userDto.getSex());
//        userEntity.setDateOfBirth(userDto.getDateOfBirth());
//        userEntity.setStatus(userDto.getStatus());
//        userEntity.setCreatedDate(userDto.getCreatedDate());
//        userEntity.setModifiedDate(userDto.getModifiedDate());
//        userEntity.setCreatedBy(userDto.getCreatedBy());
//        userEntity.setModifiedBy(userDto.getModifiedBy());
        return  userEntity;
    }

}

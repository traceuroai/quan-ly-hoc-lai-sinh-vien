package com.project.qlht.converter;

import org.springframework.stereotype.Component;

import com.project.qlht.dto.AddressDto;
import com.project.qlht.entity.Address;

@Component
public class AddressConverter {

	public Address toAddress(AddressDto dto)
	{
		Address addressEntity = new Address();
		addressEntity.setId(dto.getId());
		addressEntity.setAddress(dto.getAddress());
		addressEntity.setCreatedDate(dto.getCreateddate());
		addressEntity.setCreatedBy(dto.getCreatedBy());
		addressEntity.setModifiedDate(dto.getModifiedDate());
		addressEntity.setModifiedBy(dto.getModifiedBy());
		return addressEntity;
	}
	
}

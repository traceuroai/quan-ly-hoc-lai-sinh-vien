package com.project.qlht.converter;

import com.project.qlht.dto.detail.UserDetailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.project.qlht.entity.User;
import org.springframework.util.DigestUtils;

import java.net.PasswordAuthentication;

@Component
public class UserConverter {

    @Autowired
    ClassConverter classConverter;

    @Autowired
    PasswordEncoder passwordEncoder;

    public User toUser(UserDetailDto userDetailDto)
    {
        User userEntity = new User();
        userEntity.setId(userDetailDto.getId());
        userEntity.setUsername(userDetailDto.getUserName());
        userEntity.setCode(userDetailDto.getCode());
        userEntity.setFullName(userDetailDto.getFullName());
        userEntity.setPassword(passwordEncoder.encode(userDetailDto.getPassword()));
        userEntity.setPhone(userDetailDto.getPhone());
        userEntity.setAddress(userDetailDto.getAddress());
        userEntity.setSex(userDetailDto.getSex());
        userEntity.setDateOfBirth(userDetailDto.getDateOfBirth());
        userEntity.setStatus(userDetailDto.getStatus());
        userEntity.setCreatedDate(userDetailDto.getCreatedDate());
        userEntity.setModifiedDate(userDetailDto.getModifiedDate());
        userEntity.setCreatedBy(userDetailDto.getCreatedBy());
        userEntity.setModifiedBy(userDetailDto.getModifiedBy());
        userEntity.setClassEntity(userDetailDto.getClassDto() != null ? classConverter.toClass(userDetailDto.getClassDto()) : null);
        return userEntity;
    }

}

package com.project.qlht.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.qlht.dto.StudytimeDto;
import com.project.qlht.entity.Studytime;

@Component
public class StudytimeConverter {

    @Autowired
    private ClassscheduleConverter classscheduleConverter;

    public Studytime toStudyTime(StudytimeDto studytimeDto)
    {
        Studytime studytimeEntity = new Studytime();
        studytimeEntity.setId(studytimeDto.getId());
        studytimeEntity.setSessions(studytimeDto.getSessions());
        studytimeEntity.setStuff(studytimeDto.getStuff());
        studytimeEntity.setDays(studytimeDto.getDays());
        studytimeEntity.setTimes(studytimeDto.getTimes());
//        studytimeEntity.setClassSchedule(classscheduleConverter.toClassSchedule(studytimeDto.getClassscheduleDto()));
        return studytimeEntity;
    }
	
}

package com.project.qlht.converter;

import com.project.qlht.dto.AddressDto;
import com.project.qlht.dto.SubjectDto;
import com.project.qlht.dto.UserDto;
import com.project.qlht.dto.detail.ClassScheduleDetailDto;
import com.project.qlht.entity.Address;
import com.project.qlht.entity.ClassSchedule;
import com.project.qlht.entity.Subject;
import com.project.qlht.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ClassscheduleConverter {

    @Autowired
    AddressConverter addressConverter;
    @Autowired
    SubjectConverter subjectConverter;
    @Autowired
    StudytimeConverter studytimeConverter;

    public ClassSchedule toClassSchedule(ClassScheduleDetailDto classScheduleDetailDto) {
        ClassSchedule classscheduleEntity = new ClassSchedule();
        classscheduleEntity.setId(classScheduleDetailDto.getId());
        classscheduleEntity.setSubjectClassSchedule(classScheduleDetailDto.getSubjectDto() != null ?
                toSubject(classScheduleDetailDto.getSubjectDto()) : null);
        classscheduleEntity.setTerm(classScheduleDetailDto.getTerm());
        classscheduleEntity.setClasses(classScheduleDetailDto.getClasses());
        classscheduleEntity.setCourse(classScheduleDetailDto.getCourse());
        classscheduleEntity.setAmount(classScheduleDetailDto.getAmount());
        classscheduleEntity.setLessonNumber(classScheduleDetailDto.getLessonNumber());
        classscheduleEntity.setAddressEntity(classScheduleDetailDto.getAddress() != null ?
                toAddress(classScheduleDetailDto.getAddress()) : null);
        classscheduleEntity.setUserClassSchedule(classScheduleDetailDto.getTeacher() != null ?
                toUser(classScheduleDetailDto.getTeacher()) : null);
        classscheduleEntity.setNote(classScheduleDetailDto.getNote());
        return classscheduleEntity;
    }

    public Subject toSubject(SubjectDto subjectDto) {
        Subject subjectEntity = new Subject();
        subjectEntity.setId(subjectDto.getId());
        return subjectEntity;
    }

    public Address toAddress(AddressDto addressDto) {
        Address addressEntity = new Address();
        addressEntity.setId(addressDto.getId());
        return addressEntity;
    }

    public User toUser(UserDto userDto) {
        User userEntity = new User();
        userEntity.setId(userDto.getId());
        return userEntity;
    }

}

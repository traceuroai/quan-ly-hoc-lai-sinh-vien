package com.project.qlht.converter;

import com.project.qlht.dto.UserDto;
import com.project.qlht.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.project.qlht.dto.TranscriptDto;
import com.project.qlht.entity.Transcript;

@Component
public class TranscriptConverter {

    @Autowired
    SubjectConverter subjectConverter;

    public Transcript toTranscript(TranscriptDto transcriptDto)
    {
        Transcript transcriptEntity = new Transcript();
        transcriptEntity.setId(transcriptDto.getId());
        transcriptEntity.setTranscriptTeacher(toUser(transcriptDto.getTeacher()));
        return transcriptEntity;
    }

    public User toUser(UserDto userDto)
    {
        User userEntity = new User();
        userEntity.setId(userDto.getId());
        userEntity.setUsername(userDto.getUserName());
        userEntity.setFullName(userDto.getFullName());
//        userEntity.setPassword(userDto.getPassword());
        userEntity.setPhone(userDto.getPhone());
        userEntity.setAddress(userDto.getAddress());
        userEntity.setSex(userDto.getSex());
        userEntity.setDateOfBirth(userDto.getDateOfBirth());
        userEntity.setStatus(userDto.getStatus());
        userEntity.setCreatedDate(userDto.getCreatedDate());
        userEntity.setModifiedDate(userDto.getModifiedDate());
        userEntity.setCreatedBy(userDto.getCreatedBy());
        userEntity.setModifiedBy(userDto.getModifiedBy());
        return  userEntity;
    }
	
	
}

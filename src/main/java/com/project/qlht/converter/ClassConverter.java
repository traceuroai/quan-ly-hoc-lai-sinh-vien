package com.project.qlht.converter;

import org.springframework.stereotype.Component;

import com.project.qlht.dto.ClassDto;
import com.project.qlht.entity.Class;

@Component
public class ClassConverter {

	public Class toClass(ClassDto classDto)
	{
		Class classEntity = new Class();
		classEntity.setId(classDto.getId());
		classEntity.setCode(classDto.getCode());
		classEntity.setName(classDto.getName());
		classEntity.setCourse(classDto.getCourse());
		classEntity.setCreatedDate(classDto.getCreateddate());
		classEntity.setCreatedBy(classDto.getCreatedBy());
		classEntity.setModifiedDate(classDto.getModifiedDate());
		classEntity.setModifiedBy(classDto.getModifiedBy());
		return classEntity;
	}
	
}

package com.project.qlht.converter;

import org.springframework.stereotype.Component;

import com.project.qlht.dto.SubjectDto;
import com.project.qlht.entity.Subject;

@Component
public class SubjectConverter {

    public Subject toSubject(SubjectDto subjectDto)
    {
        Subject subjectEntity = new Subject();
        subjectEntity.setId(subjectDto.getId());
        subjectEntity.setCode(subjectDto.getCode());
        subjectEntity.setName(subjectDto.getName());
        subjectEntity.setCredits(subjectDto.getCredits());
        subjectEntity.setTerm(subjectDto.getTerm());
        subjectEntity.setCreatedDate(subjectDto.getCreatedDate());
        subjectEntity.setModifiedDate(subjectDto.getModifiedDate());
        subjectEntity.setCreatedBy(subjectDto.getCreatedBy());
        subjectEntity.setModifiedBy(subjectDto.getModifiedBy());
        return subjectEntity;
    }
	
}

package com.project.qlht.converter;

import com.project.qlht.dto.ClassDto;
import com.project.qlht.dto.CourseDto;
import com.project.qlht.entity.Class;
import com.project.qlht.entity.Course;
import org.springframework.stereotype.Component;

@Component
public class CourseConverter {

    public Course toCourse(CourseDto courseDto) {
        Course course = new Course();
        course.setId(courseDto.getId());
        course.setCode(courseDto.getCode());
        course.setName(courseDto.getName());
        return course;
    }

}

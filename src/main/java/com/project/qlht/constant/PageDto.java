package com.project.qlht.constant;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
public class PageDto<T> {
	private Integer pageIndex;
	private Integer pageSize;
	private int totalPages;
	private int totalRecords;
	private int beginIndex;
	private int endIndex;
	private List<T> data;

	public static <T> PageDto<T> of(Page page, List<T> list) {
		PageDto<T> pageData = new PageDto<>();
		pageData.setPageIndex(page.getPageable().getPageNumber() + 1);
		pageData.setPageSize(page.getPageable().getPageSize());
		pageData.setBeginIndex(Math.toIntExact(page.getPageable().getOffset()));
		pageData.setEndIndex(Math.toIntExact(page.getPageable().getOffset() + page.getNumberOfElements()));
		pageData.setTotalPages(page.getTotalPages());
		pageData.setTotalRecords(Math.toIntExact(page.getTotalElements()));
		pageData.setData(list);
		return pageData;
	}


    @Override
	public String toString() {
		return "Page{" + "pageIndex=" + pageIndex + ", pageSize=" + pageSize + ", totalPages=" + totalPages
				+ ", totalRecords=" + totalRecords + ", beginIndex=" + beginIndex + ", endIndex=" + endIndex
				+ ", data.size=" + (data == null ? null : data.size()) + '}';
	}
}

package com.project.qlht.constant;

public class SystemConstant {
	
	public static final String Giao_Vu = "Giao-Vu";
	public static final String Giao_Vien = "Giao-Vien";
	public static final String Sinh_Vien = "Sinh-Vien";
	public static final String Status_Active = "ACTIVE";
	public static final String Status_Inactive = "INACTIVE";
}

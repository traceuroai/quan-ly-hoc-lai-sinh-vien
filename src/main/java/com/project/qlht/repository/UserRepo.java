package com.project.qlht.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.qlht.entity.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRepo extends JpaRepository<User, Long>,
		JpaSpecificationExecutor<User> {
	
	User findOneById(Long id);
	
	User findByUsername(String username);
	
	User findByUsernameAndPasswordAndStatus(String username, String password, String status);

	Boolean existsByUsernameAndPhone(String userName, String phone);

	Boolean existsByFullNameAndPhoneAndRole_Id(String fullName, String phone, Long roleId);

	boolean existsByClassEntity_Id(Long classId);

}

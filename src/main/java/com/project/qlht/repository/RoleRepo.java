package com.project.qlht.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.qlht.entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RoleRepo extends JpaRepository<RoleEntity, Long>,
		JpaSpecificationExecutor<RoleEntity> {
	
	RoleEntity findOneById(Long id);

}

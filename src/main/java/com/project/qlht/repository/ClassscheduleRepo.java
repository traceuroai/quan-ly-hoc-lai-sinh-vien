package com.project.qlht.repository;

import com.project.qlht.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import com.project.qlht.entity.ClassSchedule;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ClassscheduleRepo extends JpaRepository<ClassSchedule, Long>,
		JpaSpecificationExecutor<ClassSchedule> {
	
	ClassSchedule findBySubjectClassSchedule(Subject subject);

	boolean existsBySubjectClassSchedule(Subject subject);
}

package com.project.qlht.repository;

import com.project.qlht.entity.TranscriptDetailEntity;
import com.project.qlht.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TranscriptDetailRepo extends JpaRepository<TranscriptDetailEntity, Long>,
        JpaSpecificationExecutor<TranscriptDetailEntity> {

    TranscriptDetailEntity findByUser(User user);

    TranscriptDetailEntity findOneById(Long id);

    @Query("select t from TranscriptDetailEntity t where t.transcript.id = :transcriptId")
    List<TranscriptDetailEntity> findAllByTranscript(@Param("transcriptId") Long transcriptId);

    @Query("select t from TranscriptDetailEntity t where t.transcript.id = :transcriptId and t.user.id = :userId")
    TranscriptDetailEntity searchTranscript(@Param("transcriptId") Long transcriptId, @Param("userId") Long userId);

}

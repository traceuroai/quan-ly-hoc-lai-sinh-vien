package com.project.qlht.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.project.qlht.entity.Class;

import java.util.List;

public interface ClassRepo extends JpaRepository<Class, Long>, JpaSpecificationExecutor<Class> {
	
	Class findOneById(Long id);

	boolean existsByNameAndCourse(String name, String course);

	List<Class> findAllByCourseEntity_Id(Long courseId);

	boolean existsByCourseEntity_Id(Long courseId);

}

package com.project.qlht.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.qlht.entity.Address;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AddressRepo extends JpaRepository<Address, Long>, JpaSpecificationExecutor<Address> {

	boolean existsByAddress(String addressName);

}

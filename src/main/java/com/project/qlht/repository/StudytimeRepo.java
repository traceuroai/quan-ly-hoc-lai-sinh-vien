package com.project.qlht.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.qlht.entity.Studytime;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StudytimeRepo extends JpaRepository<Studytime, Long>,
		JpaSpecificationExecutor<Studytime> {
	
	Studytime findOneById(Long id);

	@Modifying
	@Query("delete from Studytime where classSchedule.id = :classScheduleId")
	void deleteAllByClassSchedule(@Param("classScheduleId") Long classScheduleId);

}

package com.project.qlht.repository;

import com.project.qlht.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import com.project.qlht.entity.Register;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RegisterRepo extends JpaRepository<Register, Long>,
		JpaSpecificationExecutor<Register> {
	
	Register findOneById(Long id);

	@Query("select u from User u where u in ( select r.userEntity.id from Register r where r.subjectEntity.id = :subjectId)")
	List<User> findAllUser(@Param("subjectId") Long subjectId);

	boolean existsByUserEntity_IdAndSubjectEntity_Id(Long userId, Long SubjectId);

	boolean existsBySubjectEntity_Id(Long subjectId);
	
}

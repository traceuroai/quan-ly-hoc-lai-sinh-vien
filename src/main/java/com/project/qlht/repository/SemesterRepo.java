package com.project.qlht.repository;

import com.project.qlht.entity.Semester;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SemesterRepo extends JpaRepository<Semester, Long>, JpaSpecificationExecutor<Semester> {

    boolean existsByName(String name);

}

package com.project.qlht.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.qlht.entity.Transcript;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TranscriptRepo extends JpaRepository<Transcript, Long>,
		JpaSpecificationExecutor<Transcript> {
	
	Transcript findOneById(Long id);

	@Modifying
	@Query("update Transcript t set t.subjectTranscript.id = :subjectId where t.subjectTranscript.id =:idSubject")
    Transcript updateSubjectTranscript(@Param("subjectId") Long subjectId, @Param("idSubject") Long idSubject);

	@Query("select t from Transcript t where t.subjectTranscript.id = :subjectId")
    Transcript findBySubjectTranscript(@Param("subjectId") Long subjectId);

}

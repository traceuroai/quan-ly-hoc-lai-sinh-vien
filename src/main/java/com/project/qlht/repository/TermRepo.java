package com.project.qlht.repository;

import com.project.qlht.entity.Term;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TermRepo extends JpaRepository<Term, Long>, JpaSpecificationExecutor<Term> {
}

package com.project.qlht.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.qlht.entity.Subject;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface SubjectRepo extends JpaRepository<Subject, Long>,
		JpaSpecificationExecutor<Subject> {
	
	Subject findOneById(Long id);

	boolean existsByNameAndTerm(String name, String term);

	boolean existsByTermEntity_Id(Long termId);

	boolean existsBySemesterEntity_Id(Long id);

}

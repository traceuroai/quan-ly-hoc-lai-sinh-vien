package com.project.qlht.dto;

import lombok.Data;

@Data
public class ResponseHttp {
	
	private String message;
    private int code;
    private String stringResponse;
    public ResponseHttp(String message, int code){
        this.code = code;
        this.message = message;
        this.stringResponse = "{ \"code\" : \""+code+"\", \"message\" : \""+message+"\" }";
    }

}

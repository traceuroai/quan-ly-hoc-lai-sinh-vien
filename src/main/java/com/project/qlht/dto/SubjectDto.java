package com.project.qlht.dto;

import com.project.qlht.entity.Semester;
import com.project.qlht.entity.Subject;
import com.project.qlht.entity.Term;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class SubjectDto {

	private Long id;
	@NotNull
	private String code;
	@NotNull
	private String name;
	private Integer credits;
	private String term;
	private Date createdDate;
	private Date modifiedDate;
	private String createdBy;
	private String modifiedBy;
	private SemesterDto semesterDto;
	private TermDto termDto;

	public static Builder builder ()
	{
		return new Builder();
	}

	public static class Builder
	{
		private Long id;
		private String code;
		private String name;
		private Integer credits;
		private String term;
		private Date createdDate;
		private Date modifiedDate;
		private String createdBy;
		private String modifiedBy;
		private SemesterDto semesterDto;
		private TermDto termDto;

		public Builder subject(Subject subjectEntity)
		{
			this.id = subjectEntity.getId();
			this.code = subjectEntity.getCode();
			this.name = subjectEntity.getName();
			this.credits = subjectEntity.getCredits();
			this.term = subjectEntity.getTerm();
			this.createdDate = subjectEntity.getCreatedDate();
			this.modifiedDate = subjectEntity.getModifiedDate();
			this.createdBy = subjectEntity.getCreatedBy();
			this.modifiedBy = subjectEntity.getModifiedBy();
			return this;
		}

		public Builder semester(Semester semester)
		{
			this.semesterDto = semester != null ? SemesterDto.builder().semester(semester).build() : null;
			return this;
		}

		public Builder term(Term term)
		{
			this.termDto = term != null ? TermDto.builder().term(term).build() : null;
			return this;
		}

		public SubjectDto build()
		{
			return new SubjectDto(id, code, name, credits, term, createdDate,
					modifiedDate, createdBy, modifiedBy, semesterDto, termDto);
		}

	}

}

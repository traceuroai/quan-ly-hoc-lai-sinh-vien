package com.project.qlht.dto;

import com.project.qlht.entity.Class;
import com.project.qlht.entity.Course;
import com.project.qlht.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@ToString
public class ClassDto{

	private Long id;
	@NotNull
	private String code;
	@NotNull
	private String name;
	@NotNull
	private String course;
	private Date createddate;
	private String createdBy;
	private Date modifiedDate;
	private String modifiedBy;
	private List<UserDto> userDtos;
	private CourseDto courseDto;

	public static Builder builder()
	{
		return new Builder();
	}

	public static class Builder
	{
		private Long id;
		private String code;
		private String name;
		private String course;
		private Date createddate;
		private String createdBy;
		private Date modifiedDate;
		private String modifiedBy;
		private List<UserDto> userDtos;
		private CourseDto courseDto;

		public Builder classes(Class classEntity)
		{
			this.id = classEntity.getId();
			this.code = classEntity.getCode();
			this.name = classEntity.getName();
			this.course = classEntity.getCourse();
			this.createddate = classEntity.getCreatedDate();
			this.createdBy = classEntity.getCreatedBy();
			this.modifiedDate = classEntity.getModifiedDate();
			this.modifiedBy = classEntity.getModifiedBy();
			return this;
		}

		public Builder user(List<User> users)
		{
			this.userDtos = users != null && !users.isEmpty() ? users.stream().map(user -> UserDto.builder().user(user)
					.build()).collect(Collectors.toList()) : new ArrayList<>();
			return this;
		}

		public Builder course(Course course)
		{
			this.courseDto = course != null ? CourseDto.builder().course(course)
					.build() : null;
			return this;
		}

		public ClassDto build()
		{
			return new ClassDto(id, code, name, course, createddate, createdBy, modifiedDate, modifiedBy, userDtos, courseDto);
		}

	}

}

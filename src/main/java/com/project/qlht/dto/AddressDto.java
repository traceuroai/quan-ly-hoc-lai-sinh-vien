package com.project.qlht.dto;

import com.project.qlht.entity.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class AddressDto{

	private Long id;
	@NotNull
	private String address;
	private Date createddate;
	private String createdBy;
	private Date modifiedDate;
	private String modifiedBy;

	public static Builder builder()
	{
		return new Builder();
	}

	public static class Builder
	{
		private Long id;
		private String address;
		private Date createddate;
		private String createdBy;
		private Date modifiedDate;
		private String modifiedBy;

		public Builder address(Address addressEntity)
		{
			this.id = addressEntity.getId();
			this.address = addressEntity.getAddress();
			this.createddate = addressEntity.getCreatedDate();
			this.createdBy = addressEntity.getCreatedBy();
			this.modifiedDate = addressEntity.getModifiedDate();
			this.modifiedBy = addressEntity.getModifiedBy();
			return this;
		}

		public AddressDto build()
		{
			return new AddressDto(id, address, createddate, createdBy, modifiedDate, modifiedBy);
		}

	}

}

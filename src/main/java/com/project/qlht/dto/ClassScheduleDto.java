package com.project.qlht.dto;

import com.project.qlht.entity.Address;
import com.project.qlht.entity.ClassSchedule;
import com.project.qlht.entity.Subject;
import com.project.qlht.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class ClassScheduleDto {

    private Long id;
    private String term;
    private String classes;
    private String course;
    private int amount;
    private int lessonNumber;
    private String note;
    private Date createdDate;
    private Date modifiedDate;
    private String createdBy;
    private String modifiedBy;
    private Long idSubject;
    private Long idAddress;
    private Long idTeacher;
    private SubjectDto subject;
    private AddressDto address;
    private UserDto teacher;

    public static Builder builder()
	{
		return new Builder();
	}

    public static class Builder
	{
		private Long id;
		private String term;
		private String classes;
		private String course;
		private int amount;
		private int lessonNumber;
		private String note;
		private Date createdDate;
		private Date modifiedDate;
		private String createdBy;
		private String modifiedBy;
		private Long idSubject;
		private Long idAddress;
		private Long idTeacher;
		private SubjectDto subject;
		private AddressDto address;
		private UserDto teacher;

		public Builder classSchedule(ClassSchedule classscheduleEntity)
		{
			this.id = classscheduleEntity.getId();
			this.term = classscheduleEntity.getTerm();
			this.classes = classscheduleEntity.getClasses();
			this.course = classscheduleEntity.getCourse();
			this.amount = classscheduleEntity.getAmount();
			this.lessonNumber = classscheduleEntity.getLessonNumber();
			this.note = classscheduleEntity.getNote();
			this.createdDate = classscheduleEntity.getCreatedDate();
			this.modifiedDate = classscheduleEntity.getModifiedDate();
			this.createdBy = classscheduleEntity.getCreatedBy();
			this.modifiedBy = classscheduleEntity.getModifiedBy();
			this.idSubject = classscheduleEntity.getSubjectClassSchedule() != null ?
					classscheduleEntity.getSubjectClassSchedule().getId() : null;
			this.idAddress = classscheduleEntity.getAddressEntity() != null ?
					classscheduleEntity.getAddressEntity().getId() : null;
			this.idTeacher = classscheduleEntity.getUserClassSchedule() != null ?
					classscheduleEntity.getUserClassSchedule().getId() : null;
			return this;
		}

		public Builder subject(Subject subjectEntity)
		{
			this.subject = subjectEntity != null ? SubjectDto.builder().subject(subjectEntity).build() : null;
			return this;
		}

		public Builder address(Address addressEntity)
		{
			this.address = addressEntity != null ? AddressDto.builder().address(addressEntity).build() : null;
			return this;
		}

		public Builder teacher(User userEntity)
		{
			this.teacher = userEntity != null ? UserDto.builder().user(userEntity).build() : null;
			return this;
		}

		public ClassScheduleDto build()
		{
			return new ClassScheduleDto(id, term, classes, course, amount, lessonNumber,
					note, createdDate, modifiedDate, createdBy, modifiedBy, idSubject,
					idAddress, idTeacher, subject, address, teacher);
		}

	}

}

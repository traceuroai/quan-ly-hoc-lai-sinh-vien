package com.project.qlht.dto;

import com.project.qlht.entity.Subject;
import com.project.qlht.entity.Term;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class TermDto {

    private Long id;
    @NotNull
    private String code;
    @NotNull
    private String name;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Long id;
        @NotNull
        private String code;
        @NotNull
        private String name;

        public Builder term(Term term) {
            this.id = term.getId();
            this.code = term.getCode();
            this.name = term.getName();
            return this;
        }

        public TermDto build() {
            return new TermDto(id, code, name);
        }

    }

}

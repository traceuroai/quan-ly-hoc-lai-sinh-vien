package com.project.qlht.dto;

import com.project.qlht.entity.Class;
import com.project.qlht.entity.Course;
import com.project.qlht.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class CourseDto {

    private Long id;
    private String code;
    private String name;
    private String fromDate;
    private String toDate;
    private List<ClassDto> classDto;

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private Long id;
        private String code;
        private String name;
        private String fromDate;
        private String toDate;
        private List<ClassDto> classDto;

        public Builder course(Course course)
        {
            this.id = course.getId();
            this.code = course.getCode();
            this.name = course.getName();
            this.fromDate = course.getFromDate();
            this.toDate = course.getToDate();
            return this;
        }

        public Builder classes(List<Class> classes)
        {
            this.classDto = classes != null && !classes.isEmpty() ? classes.stream().map(cl -> ClassDto.builder().classes(cl)
                    .build()).collect(Collectors.toList()) : new ArrayList<>();
            return this;
        }

        public CourseDto build()
        {
            return new CourseDto(id, code, name, fromDate, toDate, classDto);
        }

    }

}

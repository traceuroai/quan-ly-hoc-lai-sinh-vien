package com.project.qlht.dto;

import java.util.Date;

import com.project.qlht.entity.ClassSchedule;
import com.project.qlht.entity.Studytime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class StudytimeDto{

	private Long id;
	private String sessions;
	private String stuff;
	private Date days;
	private String times;
	private Date createdDate;
	private Date modifiedDate;
	private String createdBy;
	private String modifiedBy;
	private Long idClassSchedule;
	private ClassScheduleDto classscheduleDto;

	public static Builder builder ()
	{
		return new Builder();
	}

	public static class Builder
	{
		private Long id;
		private String sessions;
		private String stuff;
		private Date days;
		private String times;
		private Date createdDate;
		private Date modifiedDate;
		private String createdBy;
		private String modifiedBy;
		private Long idClassSchedule;
		private ClassScheduleDto classscheduleDto;

		public Builder studyTime(Studytime studytimeEntity)
		{
			this.id = studytimeEntity.getId();
			this.sessions = studytimeEntity.getSessions();
			this.stuff = studytimeEntity.getStuff();
			this.days = studytimeEntity.getDays();
			this.times = studytimeEntity.getTimes();
			this.createdDate = studytimeEntity.getCreatedDate();
			this.modifiedDate = studytimeEntity.getModifiedDate();
			this.createdBy = studytimeEntity.getCreatedBy();
			this.modifiedBy = studytimeEntity.getModifiedBy();
			this.idClassSchedule = studytimeEntity.getClassSchedule().getId();
			return this;
		}

		public Builder classSchedule(ClassSchedule classscheduleEntity)
		{
			this.classscheduleDto = ClassScheduleDto.builder().classSchedule(classscheduleEntity).build();
			return this;
		}

		public StudytimeDto build()
		{
			return new StudytimeDto(id, sessions, stuff, days, times, createdDate, modifiedDate,
					createdBy, modifiedBy, idClassSchedule, classscheduleDto);
		}

	}


}

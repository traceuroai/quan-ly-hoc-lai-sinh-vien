package com.project.qlht.dto;

import com.project.qlht.entity.Semester;
import com.project.qlht.entity.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@ToString
public class SemesterDto {

    private Long id;
    private String name;
    List<SubjectDto> subjectDtos;

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private Long id;
        private String name;
        List<SubjectDto> subjectDtos;

        public Builder semester(Semester semester)
        {
            this.id = semester.getId();
            this.name = semester.getName();
            return this;
        }

        public Builder subject(List<Subject> subjects)
        {
            this.subjectDtos = subjects != null && subjects.isEmpty() ? subjects.stream().map(subject ->
                    SubjectDto.builder().subject(subject).build()).collect(Collectors.toList()) : new ArrayList<>();
            return this;
        }

        public SemesterDto build()
        {
            return new SemesterDto(id, name, subjectDtos);
        }

    }

}

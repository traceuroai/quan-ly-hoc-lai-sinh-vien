package com.project.qlht.dto.detail;

import com.project.qlht.dto.SubjectDto;
import com.project.qlht.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
public class ListTranscriptDto {

    private SubjectDto subject;
    private UserDto userDto;

}

package com.project.qlht.dto.detail;

import com.project.qlht.dto.ClassDto;
import com.project.qlht.dto.RoleDto;
import com.project.qlht.dto.UserDto;
import com.project.qlht.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;


@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserDetailDto {

    private Long id;
    @NotNull
    private String userName;
    @NotNull
    private String fullName;
    @NotNull
    private String phone;
    private String password;
    private String address;
    private String sex;
    private Date dateOfBirth;
    private Integer status;
    private Date createdDate;
    private String createdBy;
    private Date modifiedDate;
    private String modifiedBy;
    private ClassDto classDto;
    private RoleDto roleDto;
    private String code;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder
    {
        private Long id;
        private String userName;
        private String fullName;
        private String phone;
        private String password;
        private String address;
        private String sex;
        private Date dateOfBirth;
        private Integer status;
        private Date createdDate;
        private String createdBy;
        private Date modifiedDate;
        private String modifiedBy;
        private ClassDto classDto;
        private RoleDto roleDto;
        private String code;

        public Builder user(User userEntity)
        {
            this.id = userEntity.getId();
            this.userName = userEntity.getUsername();
            this.fullName = userEntity.getFullName();
            this.phone = userEntity.getPhone();
            this.address = userEntity.getAddress();
            this.sex = userEntity.getSex();
            this.dateOfBirth = userEntity.getDateOfBirth();
            this.status = userEntity.getStatus();
//			this.idClass = userEntity.getClassEntity().getId();
//			this.idRole = userEntity.getRole().getId();
            this.createdDate = userEntity.getCreatedDate();
            this.modifiedDate = userEntity.getModifiedDate();
            this.createdBy = userEntity.getCreatedBy();
            this.modifiedBy = userEntity.getModifiedBy();
            this.code = userEntity.getCode();
            return this;
        }

        public UserDetailDto build()
        {
            return new UserDetailDto(id, userName, fullName, phone, password, address, sex,
                    dateOfBirth, status, createdDate, createdBy, modifiedDate, modifiedBy, classDto,
                    roleDto, code);
        }

    }

}

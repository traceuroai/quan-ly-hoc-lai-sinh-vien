package com.project.qlht.dto.detail;

import com.project.qlht.dto.UserDto;
import com.project.qlht.entity.TranscriptDetailEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class TranscriptDetailDto {

    private Long id;
    private UserDto user;
    private String fullName;
    private Date dateOfBirth;
    private Integer mark;
    private Integer numberOfSheets;
    private String note;
    private Long transcriptId;

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private Long id;
        private UserDto user;
        private String fullName;
        private Date dateOfBirth;
        private Integer mark;
        private Integer numberOfSheets;
        private String note;
        private Long transcriptId;

        public Builder transcript(TranscriptDetailEntity transcriptDetail)
        {
            this.id = transcriptDetail.getId();
            this.user = UserDto.builder().user(transcriptDetail.getUser()).build();
            this.fullName = transcriptDetail.getFullName();
            this.dateOfBirth = transcriptDetail.getDateOfBirth();
            this.mark = transcriptDetail.getMark();
            this.numberOfSheets = transcriptDetail.getNumberOfSheets();
            this.note = transcriptDetail.getNote();
            this.transcriptId = transcriptDetail.getTranscript().getId();
            return this;
        }

        public TranscriptDetailDto build()
        {
            return new TranscriptDetailDto(id, user, fullName, dateOfBirth, mark, numberOfSheets, note, transcriptId);
        }

    }

}

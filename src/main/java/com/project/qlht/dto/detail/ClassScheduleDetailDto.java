package com.project.qlht.dto.detail;

import com.project.qlht.dto.*;
import com.project.qlht.entity.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@ToString
public class ClassScheduleDetailDto {

    private Long id;
    private String term;
    private String classes;
    private String course;
    private int amount;
    private int lessonNumber;
    private Date createdDate;
    private Date modifiedDate;
    private String createdBy;
    private String modifiedBy;
    @NotNull
    private SubjectDto subjectDto;
    private UserDto teacher;
    private AddressDto address;
    private List<StudytimeDto> studytime;
    private String note;

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private Long id;
        private String term;
        private String classes;
        private String course;
        private int amount;
        private int lessonNumber;
        private Date createdDate;
        private Date modifiedDate;
        private String createdBy;
        private String modifiedBy;
        private SubjectDto subjectDto;
        private UserDto teacher;
        private AddressDto address;
        private List<StudytimeDto> studytime;
        private String note;

        public Builder classSchedule(ClassSchedule classscheduleEntity)
        {
            this.id = classscheduleEntity.getId();
            this.term = classscheduleEntity.getTerm();
            this.classes = classscheduleEntity.getClasses();
            this.course = classscheduleEntity.getCourse();
            this.amount = classscheduleEntity.getAmount();
            this.lessonNumber = classscheduleEntity.getLessonNumber();
            this.note = classscheduleEntity.getNote();
            this.createdDate = classscheduleEntity.getCreatedDate();
            this.modifiedDate = classscheduleEntity.getModifiedDate();
            this.createdBy = classscheduleEntity.getCreatedBy();
            this.modifiedBy = classscheduleEntity.getModifiedBy();
            return this;
        }

        public Builder subject(Subject subjectEntity)
        {
            this.subjectDto = subjectEntity != null ? SubjectDto.builder().subject(subjectEntity).build() : null;
            return this;
        }

        public Builder address(Address addressEntity)
        {
            this.address = addressEntity != null ? AddressDto.builder().address(addressEntity).build() : null;
            return this;
        }

        public Builder teacher(User userEntity)
        {
            this.teacher = userEntity != null ? UserDto.builder().user(userEntity).build() : null;
            return this;
        }

        public Builder studyTime(List<Studytime> studytimes)
        {
            if(studytimes != null && !studytimes.isEmpty())
            {
                this.studytime = studytimes.stream().map(studytimeDto -> StudytimeDto.builder().studyTime(studytimeDto)
                        .build()).collect(Collectors.toList());
            }else {
                this.studytime = new ArrayList<>();
            }
            return this;
        }

        public ClassScheduleDetailDto build()
        {
            return new ClassScheduleDetailDto(id, term, classes, course, amount, lessonNumber, createdDate,
                    modifiedDate, createdBy, modifiedBy, subjectDto, teacher, address, studytime, note);
        }

    }


}

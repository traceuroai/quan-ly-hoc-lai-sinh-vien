package com.project.qlht.dto;

import com.project.qlht.entity.Register;
import com.project.qlht.entity.Subject;
import com.project.qlht.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class RegisterDto{

	private Long id;
	private Long idUser;
	private Long idSubject;
	private Date createdDate;
	private Date modifiedDate;
	private String createdBy;
	private String modifiedBy;
	private SubjectDto subject;
	private UserDto user;

	public static Builder builder()
	{
		return new Builder();
	}

	public static class Builder
	{
		private Long id;
		private Long idUser;
		private Long idSubject;
		private Date createdDate;
		private Date modifiedDate;
		private String createdBy;
		private String modifiedBy;
		private SubjectDto subject;
		private UserDto user;

		public Builder register(Register registerEntity)
		{
			this.id = registerEntity.getId();
			this.idUser = registerEntity.getUserEntity().getId();
			this.idSubject = registerEntity.getSubjectEntity().getId();
			this.createdDate = registerEntity.getCreatedDate();
			this.modifiedDate = registerEntity.getModifiedDate();
			this.createdBy = registerEntity.getCreatedBy();
			this.modifiedBy = registerEntity.getModifiedBy();
			return this;
		}

		public Builder subject(Subject subjectEntity)
		{
			this.subject = SubjectDto.builder().subject(subjectEntity).build();
			return this;
		}

		public Builder user(User userEntity)
		{
			this.user = UserDto.builder().user(userEntity).build();
			return this;
		}

		public RegisterDto build()
		{
			return new RegisterDto(id, idUser, idSubject, createdDate, modifiedDate,
					createdBy, modifiedBy, subject, user);
		}

	}

}

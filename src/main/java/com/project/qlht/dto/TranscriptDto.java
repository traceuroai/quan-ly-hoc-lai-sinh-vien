package com.project.qlht.dto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.project.qlht.dto.detail.TranscriptDetailDto;
import com.project.qlht.entity.Subject;
import com.project.qlht.entity.TranscriptDetailEntity;
import com.project.qlht.entity.Transcript;
import com.project.qlht.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class TranscriptDto {

	private Long id;
	private SubjectDto subjectDto;
	private UserDto teacher;
	private Date testDay;
	List<TranscriptDetailDto> transcriptDetails;

	public static Builder builder()
	{
		return new Builder();
	}

	public static class Builder
	{
		private Long id;
		private SubjectDto subject;
		private UserDto teacher;
		private Date testDay;
		List<TranscriptDetailDto> transcriptDetails;

		public Builder transcript(Transcript transcriptEntity)
		{
			this.id = transcriptEntity.getId();
			this.testDay = transcriptEntity.getTestDay();
			return this;
		}

		public Builder subject(Subject subjectEntity)
		{
			this.subject = SubjectDto.builder().subject(subjectEntity).build();
			return this;
		}

		public Builder teacher(User teacher)
		{
			this.teacher = UserDto.builder().user(teacher).build();
			return this;
		}

		public Builder transcriptDetail(List<TranscriptDetailEntity> transcripts)
		{
			this.transcriptDetails = transcripts.stream().map(transcript -> TranscriptDetailDto.builder()
					.transcript(transcript).build()).collect(Collectors.toList());
			return this;
		}

		public TranscriptDto build()
		{
			return new TranscriptDto(id, subject, teacher, testDay, transcriptDetails);
		}

	}

}

package com.project.qlht.dto;

import java.util.Date;

import com.project.qlht.common.UserJwt;
import com.project.qlht.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
public class UserDto extends UserJwt {

	private Long id;
	private String userName;
	private String fullName;
	private String phone;
	private String address;
	private String sex;
	private Date dateOfBirth;
	private Integer status;
//	private Long idClass;
//	private Long idRole;
	private Date createdDate;
	private Date modifiedDate;
	private String createdBy;
	private String modifiedBy;

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder
	{
		private Long id;
		private String userName;
		private String fullName;
		private String phone;
		private String address;
		private String sex;
		private Date dateOfBirth;
		private Integer status;
//		private Long idClass;
//		private Long idRole;
		private Date createdDate;
		private Date modifiedDate;
		private String createdBy;
		private String modifiedBy;

		public Builder user(User userEntity)
		{
			this.id = userEntity.getId();
			this.userName = userEntity.getUsername();
			this.fullName = userEntity.getFullName();
			this.phone = userEntity.getPhone();
			this.address = userEntity.getAddress();
			this.sex = userEntity.getSex();
			this.dateOfBirth = userEntity.getDateOfBirth();
			this.status = userEntity.getStatus();
//			this.idClass = userEntity.getClassEntity().getId();
//			this.idRole = userEntity.getRole().getId();
			this.createdDate = userEntity.getCreatedDate();
			this.modifiedDate = userEntity.getModifiedDate();
			this.createdBy = userEntity.getCreatedBy();
			this.modifiedBy = userEntity.getModifiedBy();
			return this;
		}

		public UserDto build()
		{
			return new UserDto(id, userName, fullName, phone, address, sex,
					dateOfBirth, status, createdDate, modifiedDate, createdBy, modifiedBy);
		}

	}

}

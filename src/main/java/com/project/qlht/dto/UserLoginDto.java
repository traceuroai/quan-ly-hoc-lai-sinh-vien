package com.project.qlht.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UserLoginDto {
	
	private String userName;
	private String password;

}

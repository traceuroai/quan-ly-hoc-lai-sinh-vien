package com.project.qlht.dto;

import com.project.qlht.entity.RoleEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@ToString
public class RoleDto{

	private Long id;
	private String code;
	private String name;

	public static Builder builder()
	{
		return new Builder();
	}

	public static class Builder
	{
		private Long id;
		private String code;
		private String name;

		public Builder role (RoleEntity roleEntity)
		{
			this.id = roleEntity.getId();
			this.code = roleEntity.getCode();
			this.name = roleEntity.getName();
			return this;
		}

		public RoleDto build()
		{
			return new RoleDto(id, code, name);
		}

	}

}
